﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : Reaction {

    [SerializeField] private Transform firstElevatorDoor;
    [SerializeField] private Transform secondElevatorDoor;
    [SerializeField] private Transform elevatorCarriage;
    [SerializeField] float doorAnimationSpeed = 1f;
    [SerializeField] float elevatorSpeed = 3f;
    [SerializeField] private float openDuration;
    [SerializeField] private SoundEmitter soundEmitter;
    [SerializeField] private AudioSource openAudio;
    [SerializeField] private AudioSource closeAudio;

    private bool isOpen;
    private bool alreadyOpen;
    private float timeInState;

    private void Awake() {
        GameState.AddListener("ActTwoEnabled", OnActTwoEnabled);
        GameState.AddListener("ElevatorTrigger", OnElevatorTriggered);
        GameState.AddListener("DaddyFailedEscape", OnDaddyFailedEscape);
    }

    private void OnDaddyFailedEscape() {
        // Close lift door
        Debug.Log("Closing elevator door.");
        StartCoroutine(CloseElevatorDoorOne());
    }

    private void OnElevatorTriggered() {
        StartCoroutine(MoveUp());
    }

    private IEnumerator MoveUp() {
        // Close lift door
        StartCoroutine(CloseElevatorDoorOne());
        yield return new WaitForSeconds(2);
        // Move lift up
        GetComponent<Animator>().SetTrigger("elevatorTriggered");
        yield return new WaitForSeconds(2.5f);
        // Open lift door
        StartCoroutine(OpenElevatorDoorTwo());
    }

    private void OnActTwoEnabled() {
        if (firstElevatorDoor.position.z > 2f) {
            StartCoroutine(CloseElevatorDoorOne());
        }
    }

    protected override void Update() {
        timeInState += Time.deltaTime;

        if (isOpen != CheckANDConditions()) {
            isOpen = !isOpen;
            timeInState = 0.0f;
            if (isOpen) {
                StartCoroutine(OpenElevatorDoorOne());
            } 
            //else { StartCoroutine(CloseElevatorDoor());
            //}

            if (isOpen) {
                soundEmitter.PlayOnce(openAudio);
            } else {
                soundEmitter.PlayOnce(closeAudio);
            }
        }
    }

    public IEnumerator OpenElevatorDoorOne() {
        Debug.Log("ELEVATOR DOOR: OPEN");

        if (!alreadyOpen) {
            float timer = 0f;
            Vector3 doorStart = firstElevatorDoor.localPosition;

            while (timer < doorAnimationSpeed) {
                timer += Time.deltaTime;
                Vector3 slerpElevatorPos = Vector3.Slerp(doorStart, new Vector3(doorStart.x, doorStart.y, doorStart.z + 3.3f), timer / doorAnimationSpeed);
                firstElevatorDoor.localPosition = slerpElevatorPos;
                yield return new WaitForEndOfFrame();
            }
        alreadyOpen = true;
        }
    }

    public IEnumerator CloseElevatorDoorOne() {
        Debug.Log("ELEVATOR DOOR: CLOSE");
        if (alreadyOpen) {
            float timer = 0f;
            Vector3 doorStart = firstElevatorDoor.localPosition;

            while (timer < doorAnimationSpeed) {
                timer += Time.deltaTime;
                Vector3 slerpElevatorPos = Vector3.Slerp(doorStart, new Vector3(doorStart.x, doorStart.y, doorStart.z - 3.3f), timer / doorAnimationSpeed);
                firstElevatorDoor.localPosition = slerpElevatorPos;
                yield return new WaitForEndOfFrame();
            }
            alreadyOpen = false;
        }
    }

    public IEnumerator OpenElevatorDoorTwo() {
        float timer = 0f;
        Vector3 doorStart = secondElevatorDoor.localPosition;

        while (timer < doorAnimationSpeed) {
            timer += Time.deltaTime;
            Vector3 slerpElevatorPos = Vector3.Slerp(doorStart, new Vector3(doorStart.x + 3.3f, doorStart.y, doorStart.z), timer / doorAnimationSpeed);
            secondElevatorDoor.localPosition = slerpElevatorPos;
            yield return new WaitForEndOfFrame();
        }
    }

    protected override void React() { }

}
