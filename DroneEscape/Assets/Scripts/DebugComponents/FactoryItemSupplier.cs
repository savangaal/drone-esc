﻿using UnityEngine;
using System.Collections;

public class FactoryItemSupplier : MonoBehaviour {

    [SerializeField] private GameObject mineralPrefab;

    void Update () {

        if (Input.GetKeyDown(KeyCode.F)) {
            GameObject instance = Instantiate<GameObject>(mineralPrefab);
            Factory factory = FindObjectOfType<Factory>();
            factory.Input(instance.GetComponent<Rigidbody>());
        }
	}
}
