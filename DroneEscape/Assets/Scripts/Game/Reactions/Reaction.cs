﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Reaction : MonoBehaviour {
	
    [SerializeField] private string triggerName;
    [SerializeField] private List<string> conditions = new List<string>();

    protected abstract void React();

    private void Awake() {
        if (!string.IsNullOrEmpty(triggerName)) {
            GameState.AddListener(triggerName, OnTrigger);
        }
    }

    protected virtual void Update() {
        if (!string.IsNullOrEmpty(triggerName)) { return; }
        OnTrigger();
    }

    private void OnTrigger() {
        if (!CheckANDConditions()) { return; }
        React();
    }

    protected bool CheckANDConditions() {
        foreach (string condition in conditions) {
            if (!GameState.CheckCondition(condition)) { return false; }
        }
        return true;
    }

    protected bool CheckORConditions() {
        foreach (string condition in conditions) {
            if (GameState.CheckCondition(condition)) { return true; }
        }
        return false;
    }
}
