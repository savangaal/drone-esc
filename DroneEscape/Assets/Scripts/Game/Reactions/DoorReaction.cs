﻿using UnityEngine;

public class DoorReaction : Reaction {

    [SerializeField] private GameObject doorCollider;
    [SerializeField] private Animator animator;
    [SerializeField] private float openDuration;
    [SerializeField] private SoundEmitter soundEmitter;
    [SerializeField] private AudioSource openAudio;
    [SerializeField] private AudioSource closeAudio;

    private bool isOpen;
    private float timeInState;

    protected override void Update() {
        timeInState += Time.deltaTime;

        if (isOpen != CheckORConditions()) {
            isOpen = !isOpen;
            timeInState = 0.0f;
            animator.SetBool("IsOpen", isOpen);

            if (isOpen) {
                soundEmitter.PlayOnce(openAudio);
            } else {
                soundEmitter.PlayOnce(closeAudio);
            }
        }

        doorCollider.SetActive(timeInState < openDuration);
    }

    protected override void React() { }

}
