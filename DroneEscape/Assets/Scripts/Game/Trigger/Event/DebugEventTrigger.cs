﻿using UnityEngine;
using System.Collections;

public class DebugEventTrigger : EventTrigger {

    [SerializeField] private KeyCode keyCode;

    private void Update() {
        if (!Application.isEditor) { return; }
        if (Input.GetKeyDown(keyCode)) {
            Trigger();
        }
    }

}
