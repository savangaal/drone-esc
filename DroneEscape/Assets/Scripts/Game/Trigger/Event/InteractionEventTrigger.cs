﻿using UnityEngine;
using System.Collections;

public class InteractionEventTrigger : EventTrigger {

    public void Interaction() {
        Trigger();
    }
	
}
