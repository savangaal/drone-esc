﻿using UnityEngine;
using System.Collections;

public abstract class EventTrigger : MonoBehaviour {

    [SerializeField] private string eventName;

    protected void Trigger() {
        GameState.TriggerEvent(eventName);
    }

}
