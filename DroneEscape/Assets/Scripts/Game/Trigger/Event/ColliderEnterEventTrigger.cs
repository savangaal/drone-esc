﻿using UnityEngine;
using System.Collections;

public class ColliderEnterEventTrigger : EventTrigger {

    private void OnTriggerEnter(Collider collider) {
        Trigger();
    }

}
