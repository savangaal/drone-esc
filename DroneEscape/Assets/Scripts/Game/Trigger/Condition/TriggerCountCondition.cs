﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TriggerCountCondition : MonoBehaviour {

    [SerializeField] private string conditionName;
    [SerializeField] private int requiredCount;
    [SerializeField] private bool invertCondition;

    private List<Collider> enteredColliders = new List<Collider>();

    private void OnTriggerEnter(Collider collider) {
        enteredColliders.Add(collider);
        if (enteredColliders.Count == requiredCount) {
            GameState.SetCondition(conditionName, !invertCondition);
        }
    }

    private void OnTriggerExit(Collider collider) {
        enteredColliders.Remove(collider);
        if (enteredColliders.Count == requiredCount - 1) {
            GameState.SetCondition(conditionName, invertCondition);
        }
    }

}
