﻿using UnityEngine;
using System.Collections;

public class ColliderEnterConditionTrigger : ConditionTrigger {

    private void OnTriggerEnter(Collider collider) {
        Trigger();
    }

}
