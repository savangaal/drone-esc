﻿using UnityEngine;
using System.Collections;

public abstract class ConditionTrigger : MonoBehaviour {

    private enum TriggerMode {
        TurnOn,
        TurnOff,
        Toggle
    }

    [SerializeField] private string conditionName;
	[SerializeField] private TriggerMode triggerMode;

    protected void Trigger() {
        switch (triggerMode) {
            case TriggerMode.TurnOn:
                GameState.SetCondition(conditionName, true);
                break;
            case TriggerMode.TurnOff:
                GameState.SetCondition(conditionName, false);
                break;
            case TriggerMode.Toggle:
                bool value = GameState.CheckCondition(conditionName);
                GameState.SetCondition(conditionName, !value);
                break;
        }
    }

}
