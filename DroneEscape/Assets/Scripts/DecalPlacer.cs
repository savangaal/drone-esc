﻿using System.Collections;
using UnityEngine;

public class DecalPlacer : MonoBehaviour {

    [SerializeField] private GameObject decalPrefab;
    [SerializeField] private float decalTime = 2f;

    public void SpawnDecal(RaycastHit hitInfo) {
        Debug.Log("Spawning Decal");
        GameObject decal = Instantiate(decalPrefab);
        decal.transform.position = hitInfo.point;
        decal.transform.forward = hitInfo.normal * -5f;
    }

    private IEnumerator RemoveDecal(GameObject decal) {
        yield return new WaitForSeconds(decalTime);
        Destroy(decal);
    }
}
