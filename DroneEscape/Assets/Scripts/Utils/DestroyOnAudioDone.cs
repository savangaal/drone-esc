﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class DestroyOnAudioDone : MonoBehaviour {

    private AudioSource source;

    private void Awake() {
        source = GetComponent<AudioSource>();
    }

    private void Update () {
        if (!source.isPlaying) {
            Destroy(gameObject);
        }
	}

}
