﻿using UnityEngine;
using System.Collections;

public static class AnimationHelper {

    private const string CLIP_NAME_PREFIX = "__";

    public static void CrossFade(this Animation animation, AnimationClip clip) {
        animation.CreateAnimationClipIfNeeded(clip);
        animation.CrossFade(CLIP_NAME_PREFIX + clip.name, .1f);
    }

    public static void CrossFadeQueued(this Animation animation, AnimationClip clip, QueueMode mode) {
        animation.CreateAnimationClipIfNeeded(clip);
        animation.CrossFadeQueued(CLIP_NAME_PREFIX + clip.name, .1f, mode);
    }

    private static void CreateAnimationClipIfNeeded(this Animation animation, AnimationClip clip){
        if (animation.GetClip(CLIP_NAME_PREFIX + clip.name) == null) {
            animation.AddClip(clip, CLIP_NAME_PREFIX + clip.name);
        }
    }

}