﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Craft.Utils {

    [RequireComponent(typeof(AudioSource))]
    public class ClipRandomizer : MonoBehaviour {

        [SerializeField] List<AudioClip> clips;

        private void Awake() {
            AudioSource source = GetComponent<AudioSource>();
            source.clip = clips.GetRandom();
            source.Play();
        }

    }

}