﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SoundEmitter : MonoBehaviour {

    private readonly Dictionary<string, AudioSource> loopingSounds = new Dictionary<string, AudioSource>();

    public void PlayOnce(AudioSource prefab) {
        PlaySound(prefab);
    }

    public void PlayLoop(AudioSource prefab, string loopName) {
        if (loopingSounds.ContainsKey(loopName)) {
            if (loopingSounds[loopName] != null) {
                return;
            }
        }
        AudioSource sound = PlaySound(prefab);
        sound.loop = true;
        loopingSounds.Add(loopName, sound);
    }

    public void StopLoop(string loopName) {
        if (!loopingSounds.ContainsKey(loopName)) { return; }
        if (loopingSounds[loopName] != null) {
            Destroy(loopingSounds[loopName].gameObject);
        }
        loopingSounds.Remove(loopName);
    }

    private AudioSource PlaySound(AudioSource prefab) {
        AudioSource source = Instantiate<AudioSource>(prefab);
        source.transform.SetParent(transform);
        source.transform.position = Vector3.zero;
        return source;
    }

}
