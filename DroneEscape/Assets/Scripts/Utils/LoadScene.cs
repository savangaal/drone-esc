﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

    [SerializeField] private string sceneName;
    [SerializeField] private LoadSceneMode loadingMode;

	void Awake () {
        SceneManager.LoadScene(sceneName, loadingMode);
	}
}
