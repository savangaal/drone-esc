﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ListHelper {

    public static bool AddDistinct<T>(this List<T> list, T element) {
        if (list.Contains(element)) { return false; }
        list.Add(element);
        return true;
    }

	public static T GetNext<T>(this List<T> list, T current, int offset) {
        int index = list.IndexOf(current);

        index += offset;
        while (index < 0) {
            index += list.Count;
        }
        index = index % list.Count;

        return list[index];
    }

    public static T GetRandom<T>(this List<T> list) {
        if (list.Count == 0) { return default(T); }
        int randomIndex = Random.Range(0, list.Count);
        return list[randomIndex];
    }

    public static T GetLast<T>(this List<T> list) {
        if (list.Count == 0) { return default(T); }
        return list[list.Count - 1];
    }

    public static List<T> GetAtIndices<T>(this List<T> list, List<int> indices) {
        List<T> result = new List<T>();
        foreach (int index in indices) {
            if (index < 0 || index > list.Count - 1) { continue; }
        }
        indices.ForEach(x => result.Add(list[x]));
        return result;
    }

}
