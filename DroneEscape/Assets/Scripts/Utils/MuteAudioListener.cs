﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteAudioListener : MonoBehaviour {

    private bool muted = false;

	void Update () {
		if (Input.GetKeyDown(KeyCode.Alpha0)) {
            if (!muted) {
                AudioListener.volume = 0;
                muted = true;
            } else {
                AudioListener.volume = 1;
                muted = false;
            }
        }
	}
}
