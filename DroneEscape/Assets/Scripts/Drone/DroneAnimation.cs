﻿using UnityEngine;
using System.Collections;

public class DroneAnimation : MonoBehaviour {

    [SerializeField] private AnimationClip initialIdleAnimation;

    private AnimationClip idleAnimation;
    private Animation myAnimation;

    public void SetIdle(AnimationClip clip) {
        idleAnimation = clip;
        Play(clip);
    }

    public void Play(AnimationClip clip) {
        myAnimation.CrossFade(clip);
        myAnimation.CrossFadeQueued(idleAnimation, QueueMode.CompleteOthers);
    }

    private void OnEnable() {
        myAnimation = GetComponent<Animation>();
        idleAnimation = initialIdleAnimation;
        myAnimation.CrossFade(initialIdleAnimation);
    }

}
