﻿using UnityEngine;
using System.Collections;
using System;

public class ArmAnimationComponent : MonoBehaviour {
    private Animator animator;
    public Action OnInject;
    public Action OnUnlockComplete;
    public Action OnEject;
    public Action OnAnimationComplete;

    public void PlayAnimation() {
        animator.SetTrigger("IsPlaying");
    }

    private void Awake() {
        animator = GetComponent<Animator>();
    }

	private void Animation_OnInjection() {
        FireAction(OnInject);
    }

    private void Animation_OnUnlockComplete() {
        FireAction(OnUnlockComplete);
    }

    private void Animation_OnEject() {
        FireAction(OnEject);
    }

    private void Animation_Complete() {
        FireAction(OnAnimationComplete);
    }

    private void FireAction(Action action) {
        if (action == null) return;
        action.Invoke();
    }
}
