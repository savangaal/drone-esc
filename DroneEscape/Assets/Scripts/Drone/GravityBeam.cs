﻿using UnityEngine;
using System.Collections;

public class GravityBeam : MonoBehaviour {

    public Vector3 DragPosition { get { return dragLocator.position; } }
    public bool IsDraggingPlayer { get { return draggable != null && draggable.GetComponent<PlayerDrone>() != null; } }

    [SerializeField] private Transform dragLocator;
    [SerializeField] private float playerAltitude;
    [SerializeField] private float force;

    [SerializeField] private SoundEmitter soundEmitter;
    [SerializeField] private AudioSource startSound;
    [SerializeField] private AudioSource endSound;
    [SerializeField] private AudioSource loopSound;

    private Rigidbody draggable;
    private bool wasDraggableDetectingCollisions;

    public void Drag(Rigidbody draggable) {
        this.draggable = draggable;
        
        wasDraggableDetectingCollisions = draggable.detectCollisions;
        draggable.detectCollisions = false;

        if (IsDraggingPlayer) {
            draggable.GetComponent<CharacterController>().enabled = false;
        }

        soundEmitter.PlayOnce(startSound);
        soundEmitter.PlayLoop(loopSound, "GravityLoop");
    }

    public Rigidbody Drop(){
        soundEmitter.PlayOnce(endSound);
        soundEmitter.StopLoop("GravityLoop");

        if (IsDraggingPlayer) {
            draggable.GetComponent<CharacterController>().enabled = true;
        }

        Rigidbody oldDraggable = draggable;
        oldDraggable.detectCollisions = wasDraggableDetectingCollisions;

        draggable = null;
        return oldDraggable;
    }

    public void DestroyInventory() {
        if (draggable == null) { return; }
        Destroy(draggable.gameObject);
        draggable = null;
    }

    private void FixedUpdate() {
        if (draggable == null) { return; }
        if (IsDraggingPlayer) { return; }
        Vector3 delta = dragLocator.position - draggable.transform.position;
        draggable.AddForce(delta * force);
    }

    private void Update() {
        if (draggable == null) { return; }
        if (!IsDraggingPlayer) { return; }
        draggable.transform.position = dragLocator.position + Vector3.up * playerAltitude;
    }

}
