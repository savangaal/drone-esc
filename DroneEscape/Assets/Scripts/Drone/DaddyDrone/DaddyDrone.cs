﻿using UnityEngine;
using System.Collections;
using System;

public class DaddyDrone : MonoBehaviour {

    private void Awake() {
        GameState.AddListener("ActTwoEnabled", OnActTwoEnabled);
    }

    private void OnActTwoEnabled() {
        //GameState.RemoveListener("ActTwoEnabled", OnActTwoEnabled);
        Destroy(this.gameObject);
    }

    public void PlaySound(AudioSource sourcePrefab) {
        AudioSource source = Instantiate<AudioSource>(sourcePrefab);
        source.transform.SetParent(transform, false);
        source.transform.localPosition = Vector3.zero;
    }

}
