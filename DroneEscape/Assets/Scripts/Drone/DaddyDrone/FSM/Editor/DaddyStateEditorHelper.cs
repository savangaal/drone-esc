﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public static class DaddyStateEditorHelper {

    public static void RenderInEditor(this DaddyState stateMachine) {
        DaddyStateMachine rootStateMachine = stateMachine.GetRootStateMachine();
        rootStateMachine.RenderStates(true);
    }

    private static void RenderStates(this DaddyStateMachine stateMachine, bool isRootStateMachine) {
        Handles.color = Color.black;

        if (stateMachine.DefaultState != null) {
            Handles.DrawLine(stateMachine.transform.position, stateMachine.DefaultState.transform.position);
        }

        if (isRootStateMachine) {
            UpdateIcon(stateMachine, true);
        }
        RenderTransitions(stateMachine);

        foreach (DaddyState state in stateMachine.GetStates()) {
            bool isCurrentState = stateMachine.CurrentState == state;
            UpdateIcon(state, isCurrentState);

            RenderTransitions(state);

            if (state.GetType() == typeof(DaddyStateMachine)) {
                (state as DaddyStateMachine).RenderStates(false);
            }
        }

        Handles.color = Color.white;
    }

    private static void RenderTransitions(DaddyState state) {
        foreach (DaddyStateTransition transition in state.Transitions) {
            if (transition.TargetState == null) { continue; }
            Handles.color = Color.black;
            Handles.DrawLine(state.transform.position, transition.TargetState.transform.position);
        }
        foreach (DaddyStateSetState setStateComponent in state.GetComponents<DaddyStateSetState>()) {
            if (setStateComponent.TargetState == null) { continue; }
            Handles.color = Color.black;
            Handles.DrawLine(state.transform.position, setStateComponent.TargetState.transform.position);
        }
    }

    private static void UpdateIcon(DaddyState state, bool isCurrentState) {
        IconHelper.LabelIcon labelIcon = IconHelper.LabelIcon.Gray;
        if (isCurrentState) {
            if (state.GetType() == typeof(DaddyStateMachine)) {
                labelIcon = IconHelper.LabelIcon.Purple;
            } else {
                labelIcon = IconHelper.LabelIcon.Green;
            }
        }
        IconHelper.SetIcon(state.gameObject, labelIcon);
    }

}