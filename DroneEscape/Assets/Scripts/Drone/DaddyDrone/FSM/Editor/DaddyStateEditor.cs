﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(DaddyState))][CanEditMultipleObjects]
public class DaddyStateEditor : Editor {

    private DaddyState state;

    private void Awake() {
        state = target as DaddyState;
    }

    private void OnDisable() {
        List<DaddyState> states = new List<DaddyState>(FindObjectsOfType<DaddyState>());
        states.ForEach(x => IconHelper.SetIcon(x.gameObject, IconHelper.Icon.None));
    }

    private void OnSceneGUI() {
        state.RenderInEditor();
    }

}
