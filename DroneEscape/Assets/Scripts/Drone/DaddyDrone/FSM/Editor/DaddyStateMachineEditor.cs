﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(DaddyStateMachine))]
public class DaddyStateMachineEditor : Editor {

    private DaddyStateMachine stateMachine;

    private void Awake() {
        stateMachine = (DaddyStateMachine)target;
    }

    private void OnDisable() {
        List<DaddyState> states = new List<DaddyState>(FindObjectsOfType<DaddyState>());
        states.ForEach(x => IconHelper.SetIcon(x.gameObject, IconHelper.Icon.None));
    }

    private void OnSceneGUI() {
        stateMachine.RenderInEditor();
    }

}
