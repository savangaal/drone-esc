﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class DaddyStateTransition {

    public DaddyState TargetState { get { return targetState; } }

    [Serializable]
    private class Condition {

        [SerializeField] public string ConditionName;
        [SerializeField] public bool RequiredValue;

        public bool Evaluate() {
            return GameState.CheckCondition(ConditionName) == RequiredValue;
        }
    }

    [SerializeField] private string triggerName;
    [SerializeField] private List<Condition> conditions = new List<Condition>();
    [SerializeField] private DaddyState targetState;

    private bool isEnabled;

    public void Enable() {
        if (!string.IsNullOrEmpty(triggerName)) {
            GameState.AddListener(triggerName, OnTrigger);
        }
        isEnabled = true;
    }

    public void Disable() {
        GameState.RemoveListener(triggerName, OnTrigger);
        isEnabled = false;
    }

    public void Update() {
        if (!isEnabled) { return; }
        if (!string.IsNullOrEmpty(triggerName)) { return; }
        OnTrigger();
    }

    private void OnTrigger() {
        if (!CheckConditions()) { return; }
        if (targetState == null) {
            Debug.LogError("Target state is null.");
            return;
        }
        targetState.BecomeCurrentState();
    }

    private bool CheckConditions() {
        foreach (Condition condition in conditions) {
            if (!condition.Evaluate()) { return false; }
        }
        return true;
    }

}
