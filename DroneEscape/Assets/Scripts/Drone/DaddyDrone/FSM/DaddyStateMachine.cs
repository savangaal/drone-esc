﻿using UnityEngine;
using System.Collections.Generic;

public class DaddyStateMachine : DaddyState {

    public DaddyState CurrentState { get; private set; }
    public DaddyState DefaultState { get { return defaultState; } }
    
    [SerializeField] private DaddyState defaultState;

    public void GotoState(DaddyState state) {
        if (state == CurrentState) { return; }
        if (CurrentState != null) {
            CurrentState.Exit();
        }
        CurrentState = state;
        if (CurrentState != null) {
            CurrentState.Enter();
        }
    }

    public override void Enter() {
        base.Enter();
        GotoState(defaultState);
    }

    public override void Exit() {
        GotoState(null);
        base.Exit();
    }

    public IEnumerable<DaddyState> GetStates() {
        List<DaddyState> states = new List<DaddyState>();
        foreach (Transform child in transform) {
            DaddyState state = child.GetComponent<DaddyState>();
            if (state == null) { continue; }
            states.Add(state);
        }
        return states;
    }

    public bool IsRootStateMachine() {
        return transform.parent.GetComponentInParent<DaddyStateMachine>() == null;
    }

    private void Awake() {
        if (IsRootStateMachine()) { 
            Enter();
        }
    }
	
}