﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DaddyState : MonoBehaviour {

    public IEnumerable<DaddyStateTransition> Transitions { get { return transitions; } }

    [SerializeField] private List<DaddyStateTransition> transitions = new List<DaddyStateTransition>();

    private bool isCurrentState;

    public virtual void Enter() {
        isCurrentState = true;
        transitions.ForEach(x => x.Enable());
        foreach (DaddyStateComponent component in GetComponents<DaddyStateComponent>()) {
            component.Enter();
        }
    }

    public virtual void Exit() {
        isCurrentState = false;
        transitions.ForEach(x => x.Disable());
        foreach (DaddyStateComponent component in GetComponents<DaddyStateComponent>()) {
            component.Exit();
        }
    }

    private void OnDestroy() {
        transitions.ForEach(x => x.Disable());
        transitions = null;
    }

    public void BecomeCurrentState() {
        if (isCurrentState) { return; }
        List<DaddyStateMachine> parentStateMachines = new List<DaddyStateMachine>();
        DaddyStateMachine parentStateMachine = transform.parent.GetComponentInParent<DaddyStateMachine>();
        while (parentStateMachine != null) {
            parentStateMachines.Add(parentStateMachine);
            parentStateMachine = parentStateMachine.transform.parent.GetComponentInParent<DaddyStateMachine>();
        }
        parentStateMachines.Reverse();
        for (int i = 0; i < parentStateMachines.Count; i++) {
            if (i < parentStateMachines.Count - 1) {
                parentStateMachines[i].GotoState(parentStateMachines[i + 1]);
            } else {
                parentStateMachines[i].GotoState(this);
            }
        }
    }

    public DaddyStateMachine GetRootStateMachine() {
        DaddyState parentState = transform.parent.GetComponentInParent<DaddyState>();
        if (parentState != null) {
            return parentState.GetRootStateMachine();
        } else {
            return GetComponentInParent<DaddyStateMachine>();
        }
    }

    private void Update() {
        if (!isCurrentState) { return; }
        transitions.ForEach(x => x.Update());
    }

}
