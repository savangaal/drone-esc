﻿using UnityEngine;
using System.Collections;

public class DaddyStateStopMoving : DaddyStateComponent {

    protected override void OnEnter() {
        GetComponentInParent<DaddyMovement>().Halt();
    }

}
