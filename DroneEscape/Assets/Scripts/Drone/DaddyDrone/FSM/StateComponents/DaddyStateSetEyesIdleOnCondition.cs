﻿using UnityEngine;
using System.Collections;

public class DaddyStateSetEyesIdleOnCondition : DaddyStateComponent {

    [SerializeField] private string condition;
	[SerializeField] private Texture emotionOnTrue;
    [SerializeField] private Texture emotionOnFalse;
    [SerializeField] private Texture emotionOnLeave;

    private bool wasTrue;

    protected override void OnEnter() {
        wasTrue = GameState.CheckCondition(condition);
        UpdateIdle(wasTrue ? emotionOnTrue : emotionOnFalse);
    }

    protected override void OnExit() {
        UpdateIdle(emotionOnLeave);
    }

    protected override void OnUpdate() {
        bool isTrue = GameState.CheckCondition(condition);
        if (wasTrue != isTrue) {
            wasTrue = isTrue;
            UpdateIdle(isTrue ? emotionOnTrue : emotionOnFalse);
        }
    }

    private void UpdateIdle(Texture texture) {
        FindObjectOfType<DaddyEyes>().SetIdle(texture);
    }

}
