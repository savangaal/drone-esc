﻿using UnityEngine;
using System.Collections;

public class DaddyStateTriggerEvent : DaddyStateComponent {

    [SerializeField] private string eventName;
    [SerializeField] private float delay;

    private bool triggered;

    protected override void OnEnter() {
        triggered = false;
    }

    protected override void OnUpdate() {
        if (triggered) { return; }
        if (timeInState > delay) {
            triggered = true;
            GameState.TriggerEvent(eventName);
        }
    }

}
