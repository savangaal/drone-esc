﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DaddyStateDestroyInventory : DaddyStateComponent {

    [SerializeField] private GravityBeam gravityBeam;

    protected override void OnEnter() {
        gravityBeam.DestroyInventory();
    }

}