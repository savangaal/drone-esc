﻿using UnityEngine;
using System.Collections;

public class DaddyStateSetCondition : DaddyStateComponent {

    [SerializeField] private string conditionName;
    [SerializeField] private bool valueWhenInState;

    protected override void OnEnter() {
        GameState.SetCondition(conditionName, valueWhenInState);
    }

    protected override void OnExit() {
        GameState.SetCondition(conditionName, !valueWhenInState);
    }

}
