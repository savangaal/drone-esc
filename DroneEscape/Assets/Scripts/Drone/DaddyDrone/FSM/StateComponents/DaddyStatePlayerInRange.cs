﻿using UnityEngine;
using System.Collections;

public class DaddyStatePlayerInRange : DaddyStateComponent {

    [SerializeField] private string inRangeEventName;
    [SerializeField] private string[] dispatchConditions;
    [SerializeField] private float range;

    private PlayerDrone player;
    private DaddyDrone daddy;
    private bool isInRange;

    protected override void OnEnter() {
        player = FindObjectOfType<PlayerDrone>();
        daddy = FindObjectOfType<DaddyDrone>();
        isInRange = false;
    }

    protected override void OnUpdate() {
        float distance = Vector3.Distance(player.transform.position, daddy.transform.position);
        Debug.Log("Is player outside shaft?" + GameState.CheckCondition("PlayerIsOutsideShaft"));

        if (distance < range && !isInRange && CheckConditions()) {
            isInRange = true;
            GameState.TriggerEvent(inRangeEventName);
            Debug.Log("Zap dat shit.");
        }

        if (distance >= range && isInRange) {
            isInRange = false;
        }
    }

    private bool CheckConditions() {
        foreach (string condition in dispatchConditions) {
            if (!GameState.CheckCondition(condition)) {
                return false;
            }
        }
        return true;
    }

}
