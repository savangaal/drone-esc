﻿using UnityEngine;
using System.Collections;

public class DaddyStateDropInventory : DaddyStateComponent {

    [SerializeField] private GravityBeam gravityBeam;

    protected override void OnEnter() {
        gravityBeam.Drop();
    }

}
