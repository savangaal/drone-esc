﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DaddyStatePlayAnimation : DaddyStateComponent {

    [SerializeField] private AnimationClip[] animationClips;
    [SerializeField] private float delayMin;
    [SerializeField] private float delayMax;
    [SerializeField] private bool loopWithInterval;
    [SerializeField] private bool playOnEnter;

    private List<AnimationClip> animationClipList;
    private float timeUntilNextAnimation;
    private bool isTriggered;
    private bool isInitialized;
    
    protected override void OnEnter() {
        isTriggered = false;
        if (playOnEnter) {
            PlayAnimation();
        }
        DelayNextAnimation();
    }

    protected override void OnUpdate() {
        if (isTriggered) { return; }
        timeUntilNextAnimation -= Time.deltaTime;
        if (timeUntilNextAnimation < 0.0f) {
            PlayAnimation();
            DelayNextAnimation();
        }
    }

    private void Awake() {
        InitializeIfNeeded();
    }

    private void InitializeIfNeeded() {
        if (isInitialized) { return; }
        isInitialized = true;
        animationClipList = new List<AnimationClip>(animationClips);
    }

    private void DelayNextAnimation() {
        timeUntilNextAnimation = Random.Range(delayMin, delayMax);
    }

    private void PlayAnimation() {
        InitializeIfNeeded();
        isTriggered = true;
        AnimationClip clip = animationClipList.GetRandom();
        FindObjectOfType<DaddyAnimation>().Play(clip);
    }
	
}
