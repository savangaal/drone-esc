﻿using UnityEngine;
using System.Collections;

public class DaddyStateInputProduct : DaddyStateComponent {

    [SerializeField] private GravityBeam gravityBeam;

    protected override void OnEnter() {
        Rigidbody mineral = gravityBeam.Drop();
        if (mineral == null) {
            Debug.LogError("Can't input product as daddy is not carrying any.");
        }
        ProductInput productInput = FindObjectOfType<ProductInput>();
        productInput.Input(mineral.gameObject);
    }

}
