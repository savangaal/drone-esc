﻿using UnityEngine;
using System.Collections;

public class DaddyStateCollectMineral : DaddyStateComponent {

    [SerializeField] private GravityBeam gravityBeam;

    protected override void OnEnter() {
        Dispenser dispenser = GameObject.FindObjectOfType<Dispenser>();
        Rigidbody mineral = dispenser.Claim();
        mineral.transform.position = gravityBeam.DragPosition;
        gravityBeam.Drag(mineral);
    }

}
