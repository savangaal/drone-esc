﻿using UnityEngine;
using System.Collections;

public class DaddyStatePlaySoundOnEvent : DaddyStateComponent {

    [SerializeField] private string eventName;
	[SerializeField] private AudioSource sourcePrefab;

    protected override void OnEnter() {
        GameState.AddListener(eventName, OnTrigger);
    }

    protected override void OnExit() {
        GameState.RemoveListener(eventName, OnTrigger);
    }

    private void OnTrigger() {
        FindObjectOfType<DaddyDrone>().PlaySound(sourcePrefab);
    }

}
