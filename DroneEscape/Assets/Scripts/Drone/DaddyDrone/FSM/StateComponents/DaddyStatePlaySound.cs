﻿using UnityEngine;
using System.Collections;

public class DaddyStatePlaySound : DaddyStateComponentDelayed {

	[SerializeField] private AudioSource sourcePrefab;

    protected override void OnTrigger() {
        FindObjectOfType<DaddyDrone>().PlaySound(sourcePrefab);
    }

}
