﻿using UnityEngine;
using System.Collections;

public class DaddyStatePickUpPlayer : DaddyStateComponent {

    [SerializeField] private GravityBeam gravityBeam;

    protected override void OnEnter() {
        PlayerDrone player = FindObjectOfType<PlayerDrone>();
        // TO DO Fix turn Problem
        gravityBeam.Drag(player.GetComponent<Rigidbody>());
    }

}
