﻿using UnityEngine;
using System.Collections;

public class DaddyStateMovement : DaddyStateComponent {

    [SerializeField] private string walkNodeName;
    [SerializeField] private string destinationReachedEvent;

    protected override void OnEnter() {
        WalkNode walkNode = WalkNode.Find(walkNodeName);
        GetComponentInParent<DaddyMovement>().WalkTo(walkNode, OnDestinationReached);
    }

    private void OnDestinationReached() {
        if (!isActiveState) { return; }
        if (!string.IsNullOrEmpty(destinationReachedEvent)) {
            GameState.TriggerEvent(destinationReachedEvent);
        }
    }

}
