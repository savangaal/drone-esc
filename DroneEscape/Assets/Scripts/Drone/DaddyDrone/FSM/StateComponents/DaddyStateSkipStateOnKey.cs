﻿using UnityEngine;
using System.Collections;

public class DaddyStateSkipStateOnKey : DaddyStateComponent {

	[SerializeField] private DaddyState targetState;

    [SerializeField] private KeyCode keyCode;

    protected override void OnUpdate() {
        if (!Application.isEditor) { return; }
        if (Input.GetKeyDown(keyCode)) {
            targetState.BecomeCurrentState();
        }
    }

}
