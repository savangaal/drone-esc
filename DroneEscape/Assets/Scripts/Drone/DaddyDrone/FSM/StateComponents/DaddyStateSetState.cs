﻿using UnityEngine;
using System.Collections;

public class DaddyStateSetState : DaddyStateComponent {

    public DaddyState TargetState { get { return targetState; } }

    [SerializeField] private DaddyState targetState;
    [SerializeField] private float delay;

    private bool triggered;

    protected override void OnEnter() {
        triggered = false;
    }

    protected override void OnUpdate() {
        if (triggered) { return; }
        if (timeInState > delay) {
            triggered = true;
            targetState.BecomeCurrentState();
        }
    }

}
