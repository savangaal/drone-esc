﻿using UnityEngine;
using System.Collections;

public class DaddyStateUnzapPlayer : DaddyStateComponentDelayed {

    protected override void OnTrigger() {
        PlayerDrone player = FindObjectOfType<PlayerDrone>();
        player.Unshock();
    }

}
