﻿using UnityEngine;
using System.Collections;

public abstract class DaddyStateComponentDelayed : DaddyStateComponent {

	[SerializeField] private float delay;

    private bool isTriggered;

    protected override void OnEnter() {
        isTriggered = false;
        if (delay == 0.0f) {
            Trigger();
        }
    }

    protected override void OnUpdate() {
        if (isTriggered) { return; }
        if (timeInState > delay) {
            Trigger();
        }
    }

    protected abstract void OnTrigger();

    private void Trigger() {
        isTriggered = true;
        OnTrigger();
    }

}
