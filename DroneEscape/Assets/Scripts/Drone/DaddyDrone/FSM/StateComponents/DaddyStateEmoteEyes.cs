﻿using UnityEngine;
using System.Collections;

public class DaddyStateEmoteEyes : DaddyStateComponentDelayed {

	[SerializeField] private Texture emotion;
    [SerializeField] private float duration;

    protected override void OnTrigger() {
        FindObjectOfType<DaddyEyes>().Emote(emotion, duration);
    }

}
