﻿using UnityEngine;
using System.Collections;

public class DaddyStateInputMineral : DaddyStateComponent {

    [SerializeField] private GravityBeam gravityBeam;

    protected override void OnEnter() {
        Factory factory = FindObjectOfType<Factory>();
        Rigidbody mineral = gravityBeam.Drop();
        if (mineral == null) {
            Debug.LogError("Can't input mineral as daddy is not carrying any.");
        }
        factory.Input(mineral);
    }

}
