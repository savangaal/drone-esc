﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DaddyStateTakePosition : DaddyStateComponent {

	[SerializeField] private string nodeName;
    [SerializeField] private DaddyMovement daddyMovement;

    protected override void OnEnter() {
        WalkNode node = new List<WalkNode>(FindObjectsOfType<WalkNode>()).Find(x => x.name == nodeName);
        daddyMovement.PositionAt(node);
    }

}
