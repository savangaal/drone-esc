﻿using UnityEngine;
using System.Collections;

public class DaddyStateZapPlayer : DaddyStateComponentDelayed {

    protected override void OnTrigger() {
        PlayerDrone player = FindObjectOfType<PlayerDrone>();
        player.Shock();
    }

}
