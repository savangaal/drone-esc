﻿using UnityEngine;
using System.Collections;

public class DaddyStateCollectProduct : DaddyStateComponent {

	[SerializeField] private GravityBeam gravityBeam;

    protected override void OnEnter() {
        GameObject productObject = FindObjectOfType<Factory>().Collect();
        if (productObject == null) {
            Debug.LogError("No product in factory available");
            return;
        }
        productObject.transform.position = gravityBeam.DragPosition;
        gravityBeam.Drag(productObject.GetComponent<Rigidbody>());
    }

}
