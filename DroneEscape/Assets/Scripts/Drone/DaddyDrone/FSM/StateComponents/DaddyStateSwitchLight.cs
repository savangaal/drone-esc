﻿using UnityEngine;
using System.Collections;

public class DaddyStateSwitchLight : DaddyStateComponentDelayed
{

    private LightManager lightmanager;
    [SerializeField] private GameObject lighttarget;

    protected override void OnTrigger()
    {

        lightmanager = GameObject.Find("Lighting").GetComponent<LightManager>();
        lightmanager.LightSwitch(lighttarget);

        foreach (ReflectionProbe r in lightmanager.ReflectionProbes)
        {
            r.RenderProbe();
        }
    }




}
