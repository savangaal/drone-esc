﻿using UnityEngine;
using System.Collections;

public class DaddyStateWakeUpPlayer : DaddyStateComponentDelayed {

    protected override void OnTrigger() {
        FindObjectOfType<PlayerDrone>().WakeUp();
    }

}
