﻿using UnityEngine;
using System.Collections;

public class DaddyStateSetEyesIdle : DaddyStateComponentDelayed {

	[SerializeField] private Texture emotion;

    protected override void OnTrigger() {
        FindObjectOfType<DaddyEyes>().SetIdle(emotion);
    }

}
