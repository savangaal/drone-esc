﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DaddyStateInteract : DaddyStateComponent {

    [SerializeField] private string interactionTriggerName;

    protected override void OnEnter() {
        InteractionEventTrigger interactionTrigger = new List<InteractionEventTrigger>(FindObjectsOfType<InteractionEventTrigger>()).Find(x => x.name == interactionTriggerName);
        if (interactionTrigger == null) {
            Debug.LogError("Can't find interaction-trigger with name '" + interactionTriggerName + "'", this);
        }
        interactionTrigger.Interaction();
    }

}
