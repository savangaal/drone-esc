﻿using UnityEngine;
using System.Collections;

public class DaddyStateSetLookAtPlayer : DaddyStateComponent {

    [SerializeField] private bool lookAtPlayer;

    protected override void OnEnter() {
        FindObjectOfType<DaddyLook>().SetLookAtPlayer(lookAtPlayer);
    }
	
}
