﻿using UnityEngine;
using System.Collections;

public class DaddyStateHintPlayer : DaddyStateComponent {

    private enum HintState {
        Idle,
        LookAt,
        Sideways
    }

    private const float INTERVAL = 2.0f;
    private const float DURATION = 2.0f;

	[SerializeField] private string lookTargetName;
    [SerializeField] private AnimationClip hintLeftAnimationClip;
    [SerializeField] private AnimationClip hintRightAnimationClip;
    [SerializeField] private Texture eyesLeft;
    [SerializeField] private Texture eyesLeftOffset;
    [SerializeField] private Texture eyesRight;
    [SerializeField] private Texture eyesRightOffset;
    [SerializeField] private Texture eyesMouse;
    [SerializeField] private AudioSource eyeHintAudio;
    [SerializeField] private AudioSource pointHintAudio;

    private HintState hintState;
    private float timeInHintState;
    private LookTarget lookTarget;
    private bool showedMouseLastTime;

    protected override void OnEnter() {
        lookTarget = LookTarget.Find(lookTargetName);
        timeInHintState = 0.0f;
        hintState = HintState.Idle;
    }

    protected override void OnExit() {
        FindObjectOfType<DaddyLook>().UnsetLookTarget();
    }

    protected override void OnUpdate() {
        timeInHintState += Time.deltaTime;
        switch (hintState) {
            case HintState.Idle:
                if (timeInHintState > INTERVAL) {
                    StartRandomHint();
                }
                break;
            default:
                if (timeInHintState > DURATION) {
                    timeInHintState = 0.0f;
                    if (hintState == HintState.LookAt) { FindObjectOfType<DaddyLook>().UnsetLookTarget(); }
                    hintState = HintState.Idle;
                }
                break;
        }
    }

    private void StartRandomHint() {
        timeInHintState = 0.0f;
        if (Random.value < .8f) {
            StartSidewaysHintState();
        } else {
            StartLookAtHintState();
        }
    }

    private void StartSidewaysHintState() {
        hintState = HintState.Sideways;
        bool isPlayerLeftOfTarget = IsPlayerLeftOfTarget();
        AnimationClip clip = isPlayerLeftOfTarget ? hintLeftAnimationClip : hintRightAnimationClip;
        FindObjectOfType<DaddyAnimation>().Play(clip);
        FindObjectOfType<DaddyDrone>().PlaySound(eyeHintAudio);
        StopCoroutine("AnimateEyesHinting");
        StartCoroutine("AnimateEyesHinting", isPlayerLeftOfTarget);
    }

    private void StartLookAtHintState() {
        hintState = HintState.LookAt;
        FindObjectOfType<DaddyLook>().SetLookTarget(lookTarget);
        FindObjectOfType<DaddyDrone>().PlaySound(pointHintAudio);
    }

    private bool IsPlayerLeftOfTarget() {
        PlayerDrone player = FindObjectOfType<PlayerDrone>();
        DaddyDrone daddy = FindObjectOfType<DaddyDrone>();
        Vector3 daddyToPlayer = player.transform.position - daddy.transform.position;
        Vector3 daddyToLook = lookTarget.transform.position - daddy.transform.position;
        daddyToPlayer.y = 0.0f;
        daddyToLook.y = 0.0f;
        Vector3 result = Vector3.Cross(daddyToPlayer, daddyToLook);
        return result.y > 0.0f;
    }

    private IEnumerator AnimateEyesHinting(bool pointLeft) {
        const float delay = 0f;
        const float duration = .7f;
        const float interval = .3f;

        yield return new WaitForSeconds(delay);
        DaddyEyes eyes = FindObjectOfType<DaddyEyes>();
        if (!showedMouseLastTime && eyesMouse != null) {
            showedMouseLastTime = true;
            eyes.Emote(eyesMouse, duration);
        }else {
            showedMouseLastTime = false;
            for (int i = 0; i < duration / interval; i++) {
                Texture eyesTexture = null;
                if (pointLeft) {
                    eyesTexture = i % 2 == 0 ? eyesLeft : eyesLeftOffset;
                } else {
                    eyesTexture = i % 2 == 0 ? eyesRight : eyesRightOffset;
                }

                eyes.Emote(eyesTexture, interval * 2.0f);
                yield return new WaitForSeconds(interval);
            }
        }
    }
}