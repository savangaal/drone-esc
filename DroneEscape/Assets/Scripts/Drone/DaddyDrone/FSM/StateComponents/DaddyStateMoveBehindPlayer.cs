﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DaddyStateMoveBehindPlayer : DaddyStateComponent {

    [SerializeField] private DaddyMovement daddyMovement;
    [SerializeField] private float distance;

    protected override void OnEnter() {
        Debug.Log("Daddy moves behind player.");
        PlayerLook drone = FindObjectOfType<PlayerLook>();
        Vector3 position = drone.transform.position + drone.transform.forward * -distance;
        position.y = daddyMovement.transform.position.y;
        daddyMovement.PositionAt(position);
    }

}