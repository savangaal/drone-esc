﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WalkNode : MonoBehaviour {

    [Serializable]
    public class Path {
        public WalkNode Destination;
        public List<WalkNode> Route;

        public float TotalDistance;
        public List<float> Distances;
    }

    public List<WalkNode> AccessibleNodes = new List<WalkNode>();
    public List<Path> Paths;

    public static WalkNode Find(string name) {
        WalkNode result = new List<WalkNode>(FindObjectsOfType<WalkNode>()).Find(x => x.name == name);
        if (result == null) {
            Debug.LogError("WalkNode '" + name + "' not found!");
        }
        return result;
    }

    public static WalkNode FindClosestToPosition(Vector3 position) {
        List<WalkNode> nodes = new List<WalkNode>(FindObjectsOfType<WalkNode>());
        float minDistance = float.MaxValue;
        WalkNode closestNode = null;
        foreach (WalkNode node in nodes) {
            float distance = Vector3.Distance(position, node.transform.position);
            if (closestNode == null || distance < minDistance) {
                minDistance = distance;
                closestNode = node;
            }
        }
        return closestNode;
    }

}
