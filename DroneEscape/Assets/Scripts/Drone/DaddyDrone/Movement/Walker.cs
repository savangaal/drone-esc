﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Walker : MonoBehaviour {

    public Vector3 Direction { get; private set; }
    public bool IsWalking { get; private set; }

    public float Speed;
    public string StartNodeName;

    private WalkNode currentNode;
    private bool isPaused = false;

    public void PositionAtNode(WalkNode node) {
        transform.position = node.transform.position;
        currentNode = node;
        IsWalking = false;
        StopCoroutine("WalkToNodeOverTime");
    }

    public void Pause() {
        isPaused = true;
    }

    public void Continue() {
        isPaused = false;
    }

    public void Stop() {
        IsWalking = false;
        StopCoroutine("WalkToNodeOverTime");
    }

    public void WalkToNode(WalkNode destination, Action onReached) {
        Continue();

        if (currentNode == null) {
            currentNode = destination;
        }

        if (currentNode == destination) {
            transform.position = destination.transform.position;
            if (onReached != null) {
                onReached();
            }
            return;
        }

        StopCoroutine("WalkToNodeOverTime");
        StartCoroutine("WalkToNodeOverTime", new object[] { destination, onReached });
    }

    private void Awake() {
        currentNode = WalkNode.Find(StartNodeName);
    }

    private IEnumerator WalkToNodeOverTime(object[] parameters) {
        WalkNode destination = parameters[0] as WalkNode;
        Action onReached = parameters[1] as Action;

        float timeSinceStart = 0.0f;
        
        WalkNode.Path path = currentNode.Paths.Find(x => x.Destination == destination);
        float duration = path.TotalDistance / Speed;

        List<Vector3> positions = new List<Vector3>();
        positions.Add(transform.position);
        path.Route.ForEach(x => positions.Add(x.transform.position));

        List<float> distances = new List<float>();
        float startDistance = Vector3.Distance(transform.position, currentNode.transform.position);
        distances.Add(startDistance);
        path.Distances.ForEach(x => distances.Add(x));

        float totalDistance = path.TotalDistance + startDistance;

        IsWalking = true;

        while (timeSinceStart < duration) {
            if (isPaused) {
                yield return new WaitForEndOfFrame();
                continue;
            }
            
            timeSinceStart += Time.deltaTime;

            float progress = timeSinceStart / duration;
            float easedProgress = EasingHelper.EaseOutSine(progress);
            
            float current = easedProgress * totalDistance;
            float distance = 0.0f;
            for (int i = 0; i < distances.Count; i++) {
                if (current > distance + distances[i]) {
                    distance += distances[i];
                    continue;
                }
                currentNode = path.Route[i];
                Vector3 from = positions[i];
                Vector3 to = positions[i + 1];

                Direction = (to - from).normalized;

                float innerProgress = (current - distance) / distances[i];
                transform.position = Vector3.Lerp(from, to, innerProgress);
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        transform.position = destination.transform.position;

        currentNode = destination;

        IsWalking = false;

        if (onReached != null) {
            onReached();
        }

    }

}
