﻿using UnityEngine;
using System.Collections;

public class DaddyLook : MonoBehaviour {

    [SerializeField] private float smoothFactor;
    [SerializeField] private float bendOverFactor;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private GravityBeam gravityBeam;

    private Walker walker;
    private LookTarget lookTarget;
    private bool lookAtPlayer;

    public void SetLookAtPlayer(bool lookAtPlayer) {
        this.lookAtPlayer = lookAtPlayer;
    }

    public void SetWalker(Walker walker) {
        this.walker = walker;
    }

    public void SetLookTarget(LookTarget lookTarget) {
        this.lookTarget = lookTarget;
    }

    public void UnsetLookTarget() {
        lookTarget = null;
    }

    private void FixedUpdate() {
        if (walker == null) { return; }

        Vector3 delta = (walker.transform.position - transform.position) * bendOverFactor;
        Vector3 up = new Vector3(delta.x, 1.0f, delta.z).normalized;
        Vector3 newUp = Vector3.Lerp(up, transform.forward, smoothFactor);

        Quaternion rotationNoLook = Quaternion.LookRotation(newUp, transform.up);
        Quaternion rotationWithLook;

        Vector3 lookTargetPosition;

        if (lookTarget != null) {
            lookTargetPosition = lookTarget.transform.position;
        } else if (walker.IsWalking || gravityBeam.IsDraggingPlayer) {
            lookTargetPosition = transform.position + walker.Direction;
        } else if (lookAtPlayer) {
            PlayerDrone player = FindObjectOfType<PlayerDrone>();
            lookTargetPosition = player.transform.position;
        } else {
            lookTargetPosition = transform.position - Vector3.forward;
        }

        rotationWithLook = Quaternion.LookRotation(newUp, lookTargetPosition - transform.position);

        transform.rotation = Quaternion.RotateTowards(rotationNoLook, rotationWithLook, rotationSpeed * Time.fixedDeltaTime);
    }

}
