﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System;

[CustomEditor(typeof(WalkNetwork), true)]
public class WalkNetworkEditor : Editor {

    WalkNetwork myTarget;

    public static void DrawWalkNodeNetwork(List<WalkNode> nodes, WalkNode selectedNode) {
        if (selectedNode == null) {
            foreach (WalkNode node in nodes) {
                foreach (WalkNode target in node.AccessibleNodes) {
                    Handles.color = Color.black;
                    Handles.DrawLine(node.transform.position, target.transform.position);
                }
            }
        } else {
            foreach (WalkNode.Path path in selectedNode.Paths) {
                for (int i = 1; i < path.Route.Count; i++) {
                    switch (i) {
                        case 1: Handles.color = Color.green; break;
                        case 2: Handles.color = Color.blue; break;
                        default: Handles.color = Color.black; break;
                    }
                    Handles.DrawLine(path.Route[i-1].transform.position,
                                     path.Route[i].transform.position);
                }
            }
        }
        Handles.color = Color.black;
        nodes.ForEach(x => Handles.CircleCap(0, x.transform.position, Quaternion.LookRotation(Vector3.up), .02f));
        Handles.color = Color.white;

        nodes.ForEach(x => IconHelper.SetIcon(x.gameObject, IconHelper.LabelIcon.Orange));
    }

    public override void OnInspectorGUI() {
        if (GUILayout.Button("Update nodes")) {
            myTarget.UpdateNodes();
            myTarget.Nodes.ForEach(x => EditorUtility.SetDirty(x));
        }

        DrawDefaultInspector();
    }

    private void OnDisable() {
        List<WalkNode> nodes = new List<WalkNode>(FindObjectsOfType<WalkNode>());
        nodes.ForEach(x => IconHelper.SetIcon(x.gameObject, IconHelper.Icon.None));
    }

    private void Awake() {
        myTarget = (WalkNetwork)target;
    }

    private void OnSceneGUI() {
        DrawWalkNodeNetwork(myTarget.Nodes, null);
    }

}
