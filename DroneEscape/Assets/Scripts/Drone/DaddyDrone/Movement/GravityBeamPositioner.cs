﻿using UnityEngine;
using System.Collections;

public class GravityBeamPositioner : MonoBehaviour {

    [SerializeField] private DaddyLook daddyLook;
    [SerializeField] private float distance;

    private void Update() {
        Vector3 direction = daddyLook.transform.up;
        direction.y = 0.0f;
        direction.Normalize();

        transform.position = daddyLook.transform.position + direction * distance;
    }

    private void FixedUpdate() {
        Update();
    }
	
}
