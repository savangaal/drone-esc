﻿using UnityEngine;
using System.Collections;

public class DroneEyes : MonoBehaviour {

    private const float SWITCH_DURATION = .2f;

    [SerializeField] private Texture startTexture;

    private bool isInitialized;
    private Renderer myRenderer;

    private float timeUntilIdle;
    private bool isIdle;
    private bool isFrontBuffer;
    private Texture frontBuffer;
    private Texture backBuffer;
    private Texture idleTexture;

    public void SetIdle(Texture eyes) {
        idleTexture = eyes;
        isIdle = true;
        SwitchEyes(idleTexture);
    }

    public void Emote(Texture eyes, float duration) {
        SwitchEyes(eyes);
        isIdle = false;
        timeUntilIdle = duration;
    }

    private void Awake() {
        InitializeIfNeeded();
    }

    private void Update() {
        if (!isIdle) {
            timeUntilIdle -= Time.deltaTime;
            if (timeUntilIdle <= 0.0f) {
                isIdle = true;
                SwitchEyes(idleTexture);
            }
        }
    }

    private void InitializeIfNeeded() {
        if (isInitialized) { return; }
        isInitialized = true;
        myRenderer = GetComponent<Renderer>();
        idleTexture = startTexture;
        isIdle = true;
        isFrontBuffer = true;
        myRenderer.material.SetTexture("_FrontTexture", startTexture);
        myRenderer.material.SetTexture("_BackTexture", startTexture);
    }

    private void SwitchEyes(Texture eyes) {
        InitializeIfNeeded();
        if (isFrontBuffer) {
            backBuffer = eyes;
            myRenderer.material.SetTexture("_BackTexture", backBuffer);
        } else {
            frontBuffer = eyes;
            myRenderer.material.SetTexture("_FrontTexture", frontBuffer);
        }
        StopCoroutine("SwitchEyesOverTime");
        StartCoroutine("SwitchEyesOverTime");
    }

    private IEnumerator SwitchEyesOverTime() {
        float duration = 0.0f;

        float start = isFrontBuffer ? 0.0f : 1.0f;
        float end = isFrontBuffer ? 1.0f : 0.0f;

        while (duration < SWITCH_DURATION) {
            duration += Time.deltaTime;
            float lerpFactor = Mathf.Lerp(start, end, duration / SWITCH_DURATION);
            float easedLerpFactor = EasingHelper.EaseInOutSine(lerpFactor);
            myRenderer.material.SetFloat("_LerpFactor", easedLerpFactor);
            yield return new WaitForEndOfFrame();
        }

        myRenderer.material.SetFloat("_LerpFactor", end);

        isFrontBuffer = !isFrontBuffer;
    }

}