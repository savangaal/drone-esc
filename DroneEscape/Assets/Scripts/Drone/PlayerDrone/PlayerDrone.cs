﻿using UnityEngine;
using System.Collections;
using System;
using UnityStandardAssets.CinematicEffects;

public class PlayerDrone : MonoBehaviour {

    public GameObject BrokenArmGameObject;
    public GameObject WholeArmGameObject;
    public bool HoldsChargedMineral;

    public GravityBeam GravityBeam { get; private set; }
    public DaddyDrone DaddyDrone { get; private set; }
    public PlayerLook PlayerLookController { get; private set; }
    public PlayerMovement PlayerMovement { get; private set; }
    public ArmAnimationComponent ArmAnimation { get; private set; }
    public Animator Animator { get; private set; }
    public ArmInteraction QueuedInteraction;
    public LensAberrations LensAberations { get; private set; }

    public float InteractionRange = 1f;

    [SerializeField] private SoundEmitter soundEmitter;
    [SerializeField] private AudioSource zapHurtSound;
    [SerializeField] private ParticleSystem shockedFX;

    private PlayerStateMachine stateMachine;
    private Transform spawnPosition;

    private bool isInitialized = false;
    private bool armIsBroken = false;

    private void Awake() {
        InitializeIfNeeded();
        //GameState.SetCondition("IsInActTwo", false);
    }

    public void Shock() {
        InitializeIfNeeded();
        stateMachine.GotoState<PlayerImmobilize>();
        soundEmitter.PlayOnce(zapHurtSound);
        // Play Particle Effect
        shockedFX.Play();
    }

    public void Unshock() {
        InitializeIfNeeded();
        if (armIsBroken) {
            stateMachine.GotoState<ArmBrokenState>();
        } else {
            stateMachine.GotoState<ArmIntactState>();
        }
        // Play Particle Effect
        shockedFX.Play();
    }

    public void BreakArm() {
        armIsBroken = true;
        BrokenArmGameObject.SetActive(true);
        WholeArmGameObject.SetActive(false);
        StartCoroutine(ArmBreakAnimation());
    }

    public void BeingZapped() {
        //TODO: activate glitching shader on camera
    }

    public void StopBeingZapped() {
        //TODO: activate glitching shader on camera
    }

    public void SetPlayerInputEnabled(bool enable) {
        InitializeIfNeeded();
        PlayerLookController.enabled = enable;
        PlayerMovement.enabled = enable;
    }

    public void StartInteraction() {
        stateMachine.GotoState<PlayerInteracting>();
    }

    public void InteractionDone() {
        InitializeIfNeeded();
        Unshock();
    }

    public void StartDragging() {
        stateMachine.GotoState<PlayerDragging>();
    }
    public void StopDragging() {
        stateMachine.GotoState<ArmBrokenState>();
    }

    public void WakeUp() {
        FindObjectOfType<Crosshair>().TurnOn();
        if (shockedFX.gameObject.activeInHierarchy) {
            shockedFX.Play();
        } else {
            shockedFX.gameObject.SetActive(true);
            shockedFX.Play();
        }
    }

    private void InitializeIfNeeded() {
        if (isInitialized) { return; }
        isInitialized = true;

        DaddyDrone = FindObjectOfType<DaddyDrone>();
        PlayerLookController = GetComponentInChildren<PlayerLook>();
        PlayerMovement = GetComponentInChildren<PlayerMovement>();
        ArmAnimation = GetComponentInChildren<ArmAnimationComponent>();
        Animator = GetComponentInChildren<Animator>();
        LensAberations = GetComponentInChildren<LensAberrations>();
        GravityBeam = GetComponentInChildren<GravityBeam>();

        stateMachine = gameObject.AddComponent<PlayerStateMachine>();
        stateMachine.Initialize(this);
        stateMachine.GotoState<PlayerSleeping>();

        BrokenArmGameObject.SetActive(false);
        WholeArmGameObject.SetActive(true);

        spawnPosition = GameObject.Find("PlayerSpawnAct2").transform;
    }

    private void OnActTwoEnabled() {
        BreakArm();
        Unshock();
    }

    #region Debugging
    private void Update() {
        DebugKeyInput();
    }

    private void DebugKeyInput() {
        if (Input.GetKeyDown(KeyCode.M)) {
            Shock();
        }
        if (Input.GetKeyDown(KeyCode.N)) {
            Unshock();
        }
        if (Input.GetKeyDown(KeyCode.F5)) {
            GameState.TriggerEvent("ActTwoEnabled");
            armIsBroken = true;
            BrokenArmGameObject.SetActive(true);
            WholeArmGameObject.SetActive(false);
            stateMachine.GotoState<ArmBrokenState>();
            GameState.TriggerEvent("ActTwoEnabled");
        }
        if (Input.GetKeyDown(KeyCode.P)) {
            armIsBroken = false;
            Unshock();
            BrokenArmGameObject.SetActive(false);
            WholeArmGameObject.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.F4)) {
            transform.position = spawnPosition.position;
        }
        if (Input.GetKeyDown(KeyCode.I)) {
            GameState.TriggerEvent("ProductInputFilled");
            GameState.SetCondition("IsProductInputFilled", true);
        }
    }
    #endregion

    // TODO: new arm break animation
    private IEnumerator ArmBreakAnimation() {
        yield return new WaitForSeconds(0.2f);
        yield return FadeVignette(15, 1);
        transform.position = spawnPosition.position;
        yield return FadeVignette(5, 0.2f);
        yield return FadeVignette(10, 0.4f);
        yield return FadeVignette(0, 0.1f);
        GameState.TriggerEvent("ActTwoEnabled");
    }

    private IEnumerator FadeVignette(float value, float speed) {
        int modifier = LensAberations.vignette.intensity < value ? 1 : -1;

        while ((modifier > 0 && LensAberations.vignette.intensity < value) || (modifier < 0 && LensAberations.vignette.intensity > value)) {
            LensAberations.vignette.intensity += modifier * speed;
            yield return new WaitForEndOfFrame();
        }
    }
}
