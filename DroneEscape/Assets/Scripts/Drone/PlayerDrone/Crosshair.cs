﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour {

    [SerializeField] private GameObject turnedOff;
    [SerializeField] private GameObject normal;
    [SerializeField] private GameObject hover;
    [SerializeField] private GameObject normalGlitch;
    [SerializeField] private GameObject hoverGlitch;
    [SerializeField] private float glitchIntervalMin;
    [SerializeField] private float glitchIntervalMax;
    [SerializeField] private float glitchDurationMin;
    [SerializeField] private float glitchDurationMax;

    private bool isHidden;
    private bool isHovering;
    private bool isBroken;
    private float timeUntilNextGlitch;

    public void TurnOn() {
        SetCrosshair(normal);
    }

    public void Hide() {
        if (isHidden) { return; }
        isHidden = true;
        SetCrosshair(null);
    }

    public void Show() {
        if (!isHidden) { return; }
        SetCrosshair(isHovering ? hover : normal);
        isHidden = false;
    }

    public void SetHover(bool isHovering) {
        if (this.isHovering == isHovering) { return; }
        this.isHovering = isHovering;
        timeUntilNextGlitch = 0.0f;
        if (!isHidden) {
            SetCrosshair(isHovering ? hover : normal);
        }
    }

    public void SetBroken(bool isBroken) {
        this.isBroken = isBroken;
    }

    private void Awake() {
        SetCrosshair(turnedOff);
    }

    private void Update() {
        if (isHidden) { return; }
        if (!isBroken) { return; }
        timeUntilNextGlitch -= Time.deltaTime;
        if (timeUntilNextGlitch <= 0.0f) {
            Glitch();
        }
    }

    private void SetCrosshair(GameObject crosshair) {
        turnedOff.SetActive(crosshair == turnedOff);
        normal.SetActive(crosshair == normal);
        hover.SetActive(crosshair == hover);
        normalGlitch.SetActive(crosshair == normalGlitch);
        hoverGlitch.SetActive(crosshair == hoverGlitch);
    }

    private void Glitch() {
        timeUntilNextGlitch = Random.Range(glitchIntervalMin, glitchIntervalMax);
        StopCoroutine("GlitchOverTime");
        StartCoroutine("GlitchOverTime");
    }

    private IEnumerator GlitchOverTime() {
        SetCrosshair(isHovering ? hoverGlitch : normalGlitch);
        yield return new WaitForSeconds(Random.Range(glitchDurationMin, glitchDurationMax));
        SetCrosshair(isHovering ? hover : normal);
    }

}
