﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    [SerializeField] private CharacterController characterController;
    [SerializeField] private float acceleration;
    [SerializeField] private float maxSpeed;
    [SerializeField] private Transform directionTransform;

    private float speed;

    private void FixedUpdate() {
        if (Input.GetMouseButton(0)) {
            speed += Time.deltaTime * acceleration;
        } else {
            speed -= acceleration;
        }
        speed = Mathf.Clamp(speed, 0.0f, maxSpeed);

        Vector3 walkDirection = directionTransform.forward;
        walkDirection.y = 0.0f;
        walkDirection.Normalize();

        characterController.SimpleMove(walkDirection * speed);
    }

}
