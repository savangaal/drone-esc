﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerSleeping : PlayerState {
    protected override void OnEnterState() {
        subject.LensAberations.vignette.intensity = 1.5f;
        subject.SetPlayerInputEnabled(false);
    }

    protected override void OnExitState() {
        StartCoroutine(FadeVignette(0f));
        subject.SetPlayerInputEnabled(true);
    }

    protected override void OnUpdate() {
        
    }

    private IEnumerator FadeVignette(float value) {
        int modifier = subject.LensAberations.vignette.intensity < value ? 1 : -1;


        while ((modifier > 0 && subject.LensAberations.vignette.intensity < value) || (modifier < 0 && subject.LensAberations.vignette.intensity > value)) {
            subject.LensAberations.vignette.intensity += modifier * 0.01f;
            yield return new WaitForEndOfFrame();
        }
    }
}
