﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerDragging : PlayerState {

    protected override void OnEnterState() {


    }

    protected override void OnExitState() {
        
    }

    protected override void OnUpdate() {
        if (Input.GetMouseButtonDown(1)) {
            Vector2 midScreen = new Vector2(Screen.width * .5f, Screen.height * .5f);
            Ray ray = Camera.main.ScreenPointToRay(midScreen);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                HandleHitObject(hit.collider.gameObject, hit.distance);
            }
        }
    }

    private void HandleHitObject(GameObject gameObject, float distance) {
        GravityBeamInput gravityBeamInput = gameObject.GetComponent<GravityBeamInput>();
        if (gravityBeamInput == null || (gravityBeamInput.acceptsOnlyOne && gravityBeamInput.isFull)) return;
        if (gravityBeamInput.AcceptsChargedMinerals != subject.HoldsChargedMineral) return;

        gravityBeamInput.Input(subject.GravityBeam.Drop());
        subject.StopDragging();
    }
}
