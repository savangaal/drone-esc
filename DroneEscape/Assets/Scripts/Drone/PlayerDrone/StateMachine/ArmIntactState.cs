﻿using UnityEngine;
using System.Collections;
using System;

public class ArmIntactState : PlayerState {

    private Crosshair crosshair;

    protected override void OnEnterState() {
        crosshair = FindObjectOfType<Crosshair>();
    }

    protected override void OnExitState() {

    }

    protected override void OnUpdate() {
        RaycastHit hit;
        GameObject hoveredObject = RaycastInteractables(out hit);
        bool isHovering = hoveredObject != null && hit.distance <= subject.InteractionRange;

        bool isHoveringValidObject = false;
        if (isHovering) {
            isHoveringValidObject = hoveredObject.GetComponent<ArmInteraction>() != null;
        }
        crosshair.SetHover(isHoveringValidObject);

        if (isHoveringValidObject && Input.GetMouseButtonDown(1)) {
            HandleHitObject(hoveredObject);
        }
    }

    private bool HandleHitObject(GameObject gameObject) {
        ArmInteraction interaction = gameObject.GetComponent<ArmInteraction>();
        if (interaction == null) { return false; }
        subject.QueuedInteraction = interaction;
        subject.StartInteraction();
        return true;
    }

}
