﻿using UnityEngine;

public class ArmBrokenState : PlayerState {

    private Crosshair crosshair;
    private DecalPlacer decalPlacer;

    protected override void OnEnterState() {
        // Change current crosshair to a broken state
        crosshair = FindObjectOfType<Crosshair>();
        crosshair.SetBroken(true);
        decalPlacer = FindObjectOfType<DecalPlacer>();
    }

    protected override void OnExitState() { }

    protected override void OnUpdate() {
        // Check if player's mouse is hovering over an object
        RaycastHit hit;
        GameObject hoveredObject = RaycastInteractables(out hit);
        bool isHovering = hoveredObject != null;

        // Check if the hovering object is a valid interactable
        bool isHoveringValidObject = false;
        if (isHovering) {
            bool isHoveringGravityBeamObject = hoveredObject.GetComponent<GrabberInteraction>() != null && hit.distance < subject.InteractionRange;
            bool isHoveringChild = hoveredObject.GetComponentInParent<ChildDrone>() != null;

            // Is child ready for an order?
            ChildDrone child  = FindObjectOfType<ChildDrone>();
            bool isChildAwaitingOrder = false;
            if (child) {
                isChildAwaitingOrder = child.IsAwaitingOrder;
            }

            isHoveringValidObject = isHoveringGravityBeamObject || isChildAwaitingOrder || isHoveringChild;
        }
        crosshair.SetHover(isHoveringValidObject);

        // Check if player clicked on interactable
        if (isHovering && Input.GetMouseButtonDown(1)) {
            HandleChildCommand(hoveredObject, hit.point, hit);
            HandleHitObject(hoveredObject, hit.distance);
        }
    }

    private void HandleChildCommand(GameObject hittedObject, Vector3 point, RaycastHit hitInfo) {
        ChildDrone child = FindObjectOfType<ChildDrone>();
        if (child == null) return;
        // Check if player clicked on a connector
        ArmInteraction interaction = hittedObject.GetComponent<ArmInteraction>();
        if (interaction != null) {
            // Make Child Drone handle connector
            child.HandleInteraction(interaction);
        } else {
            // Make Child Drone move to clicked point
            child.MoveTo(point);
            if (child.IsMoving && hittedObject.layer != LayerMask.NameToLayer("Child")) {
                decalPlacer.SpawnDecal(hitInfo);
            }
        }
    }

    private void HandleHitObject(GameObject gameObject, float distance) {
        ChildDrone child = gameObject.GetComponentInParent<ChildDrone>();
        HandleChildInteraction(child, distance);

        if (distance <= subject.InteractionRange) {
            GrabberInteraction grabber = gameObject.GetComponent<GrabberInteraction>();
            HandleGrabberInteraction(grabber);
        }
    }

    private void HandleChildInteraction(ChildDrone child, float range) {
        if (child == null) return;

        if(range >= subject.InteractionRange) {
            // get attention of drone
            child.PayAttention();
        } else {
            // zap drone
            child.Shock();
        }
    }

    private void HandleGrabberInteraction(GrabberInteraction interaction) {
        if (interaction == null) return;

        interaction.HandleInteraction();
        Rigidbody pickup = interaction.ObjectToGrab;
        pickup.transform.position = subject.GravityBeam.DragPosition;
        subject.GravityBeam.Drag(pickup);
        subject.HoldsChargedMineral = interaction.IsChargedMineral;
        subject.StartDragging();
    }
}
