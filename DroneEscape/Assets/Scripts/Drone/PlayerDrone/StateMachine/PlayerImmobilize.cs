﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerImmobilize : PlayerState {

    private class PlayerLookSettings {

        public float SensitivityX = 0;
        public float SensitivityY = 0;
        public float MinimumX = -0;
        public float MaximumX = 0;
        public float MinimumY = -0;
        public float MaximumY = 0;

        public PlayerLookSettings(PlayerLook playerLook) {
            SensitivityX = playerLook.SensitivityX;
            SensitivityY = playerLook.SensitivityY;
            MinimumX = playerLook.MinimumX;
            MaximumX = playerLook.MaximumX;
            MinimumY = playerLook.MinimumY;
            MaximumY = playerLook.MaximumY;
        }
    }

    private Transform PlayerLookTransform { get { return subject.PlayerLookController.transform; } }

    [SerializeField] private float rotationSpeed;

    private Vector3 startForward;
    private Vector3 destinationForward;
    private PlayerLookSettings oldLookSettings;

    protected override void OnEnterState() {
        oldLookSettings = new PlayerLookSettings(subject.PlayerLookController);
        ImpareLookMovement();
        subject.SetPlayerInputEnabled(false);
        StartCoroutine(FadeVignette(1.5f));
    }

    protected override void OnExitState() {
        StartCoroutine(FadeVignette(0f));
        ResetOldPlayerLookSettings();
        subject.SetPlayerInputEnabled(true);
        subject.StopBeingZapped();
    }

    private void ResetOldPlayerLookSettings() {
        subject.PlayerLookController.ResetRotation();
        subject.PlayerLookController.SensitivityX = oldLookSettings.SensitivityX;
        subject.PlayerLookController.SensitivityY = oldLookSettings.SensitivityY;
        subject.PlayerLookController.MaximumX = oldLookSettings.MaximumX;
        subject.PlayerLookController.MinimumX = oldLookSettings.MinimumX;
        subject.PlayerLookController.MaximumY = oldLookSettings.MaximumY;
        subject.PlayerLookController.MinimumY = oldLookSettings.MinimumY;
    }

    protected override void OnUpdate() {
        Vector3 direction = subject.DaddyDrone.transform.position - subject.transform.position;
        direction.y = 0.0f;

        Quaternion currentRotation = PlayerLookTransform.rotation;
        Quaternion finalRotation = Quaternion.LookRotation(direction, Vector3.up);
        PlayerLookTransform.rotation = Quaternion.RotateTowards(currentRotation, finalRotation, rotationSpeed * Time.deltaTime);

        subject.LensAberations.chromaticAberration.amount = UnityEngine.Random.Range(5, 15);
    }

    private void ImpareLookMovement() {
        subject.PlayerLookController.SensitivityX = subject.PlayerLookController.SensitivityY = 1f;
        subject.PlayerLookController.MaximumX = subject.PlayerLookController.MaximumY = 10f;
        subject.PlayerLookController.MinimumX = subject.PlayerLookController.MinimumY = -10f;
    }

    private IEnumerator FadeVignette(float value) {
        int modifier = subject.LensAberations.vignette.intensity < value ? 1 : -1;

        
        while((modifier > 0 && subject.LensAberations.vignette.intensity < value) || (modifier < 0 && subject.LensAberations.vignette.intensity > value)) {
            subject.LensAberations.vignette.intensity += modifier * 0.01f;
            yield return new WaitForEndOfFrame();
        }
    }
}
