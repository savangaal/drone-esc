﻿using UnityEngine;
using System.Collections;

public abstract class PlayerState : MonoBehaviour {

    protected PlayerDrone subject { get; private set; }
    protected bool isActiveState { get; private set; }
    protected float timeInState { get; private set; }

    public void Initialize(PlayerDrone subject) {
        this.subject = subject;
    }

    public void EnterState() {
        isActiveState = true;
        timeInState = 0.0f;
        OnEnterState();
    }

    public void ExitState() {
        isActiveState = false;
        OnExitState();
    }

    protected abstract void OnEnterState();
    protected abstract void OnExitState();
    protected abstract void OnUpdate();

    protected GameObject RaycastInteractables() {
        RaycastHit hit;
        return RaycastInteractables(out hit);
    }

    protected GameObject RaycastInteractables(out RaycastHit hit) {
        Vector2 midScreen = new Vector2(Screen.width * .5f, Screen.height * .5f);
        Ray ray = Camera.main.ScreenPointToRay(midScreen);
        if (Physics.Raycast(ray, out hit)) {
            return hit.collider.gameObject;
        }
        return null;
    }

    private void Update() {
        if (!isActiveState) { return; }
        timeInState += Time.deltaTime;
        OnUpdate();
    }

}