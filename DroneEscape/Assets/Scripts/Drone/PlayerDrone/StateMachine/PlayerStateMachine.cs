﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerStateMachine : MonoBehaviour {

    private PlayerDrone subject;
    private List<PlayerState> states = new List<PlayerState>();
    private PlayerState currentState;

    public void Initialize(PlayerDrone subject) {
        this.subject = subject;
    }

    public void GotoState<U>(bool forceRestart = false) where U : PlayerState {
        if (currentState != null) {
            if (typeof(U) == currentState.GetType() && !forceRestart) { return; }
            currentState.ExitState();
        }
        currentState = GetOrCreateState<U>();
        currentState.EnterState();
    }

    public bool IsCurrentState<U>() where U : PlayerState {
        if (currentState == null) { return false; }
        return currentState.GetType() == typeof(U);
    }

    private PlayerState GetOrCreateState<U>() where U : PlayerState {
        PlayerState state = states.Find(x => x.GetType() == typeof(U));
        if (state == null) {
            state = subject.GetComponent<U>();
            if (state == null) {
                state = subject.gameObject.AddComponent<U>();
            }
            state.Initialize(subject);
            states.Add(state);
        }
        return state;
    }

}
