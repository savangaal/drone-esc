﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerInteracting : PlayerState {

    private Vector3 originalPosition;
    private Quaternion originalRotation;
    private Quaternion localRotation;
    private Vector3 localPosition;
    private bool didInteraction = false;

    protected override void OnEnterState() {
        originalPosition = subject.PlayerLookController.transform.position;
        originalRotation = subject.PlayerLookController.transform.rotation;
        localPosition = subject.PlayerLookController.transform.localPosition;
        localRotation = subject.PlayerLookController.transform.localRotation;
        subject.ArmAnimation.OnEject += OnEject;
        subject.ArmAnimation.OnInject += OnInject;
        subject.ArmAnimation.OnUnlockComplete += OnUnlock;
        subject.ArmAnimation.OnAnimationComplete += OnCompleted;
        didInteraction = false;
        subject.SetPlayerInputEnabled(false);
        FindObjectOfType<Crosshair>().Hide();
    }

    protected override void OnExitState() {
        subject.ArmAnimation.OnEject -= OnEject;
        subject.ArmAnimation.OnInject -= OnInject;
        subject.ArmAnimation.OnUnlockComplete -= OnUnlock;
        subject.ArmAnimation.OnAnimationComplete -= OnCompleted;
        subject.SetPlayerInputEnabled(true);
        subject.Animator.SetBool("StandStill", false);
        FindObjectOfType<Crosshair>().Show();
    }

    protected override void OnUpdate() {
        if (didInteraction) return;
        Vector3 lerpedPosition = Vector3.Lerp(originalPosition, subject.QueuedInteraction.InteractionPosition.position, timeInState);
        subject.PlayerLookController.transform.LookAt(subject.QueuedInteraction.PlayerLookPosition);
        subject.PlayerLookController.transform.position = lerpedPosition;

        if (timeInState <= 1f) return;
        subject.Animator.SetBool("StandStill", true);
        didInteraction = true;
        subject.ArmAnimation.PlayAnimation();
    }

    private void OnUnlock() {
        subject.QueuedInteraction.OnInteractionDone();
        subject.QueuedInteraction.HandleInteraction();
    }

    private void OnInject() {
        subject.QueuedInteraction.OnInject();
    }

    private void OnEject() {
        subject.QueuedInteraction.OnEject();
    }

    private void OnCompleted() {
        StartCoroutine(LerpRotation());
    }

    private IEnumerator LerpRotation() {
        float timer = 0f;
        Quaternion startRotation = subject.PlayerLookController.transform.localRotation;
        Vector3 startPosition = subject.PlayerLookController.transform.localPosition;
        while (timer < 1) {
            timer += Time.deltaTime;
            Vector3 lerpedPosition = Vector3.Lerp(startPosition, localPosition, timer);
            Quaternion lerpedQuaternion = Quaternion.Lerp(startRotation, localRotation, timer);
            subject.PlayerLookController.transform.localRotation = lerpedQuaternion;
            subject.PlayerLookController.transform.localPosition = lerpedPosition;
            yield return new WaitForEndOfFrame();
        }
        //subject.PlayerLookController.ResetRotation();
        subject.InteractionDone();
    }
}
