﻿using UnityEngine;

public class ChildWalker : MonoBehaviour {

    public bool IsPaused { get; private set; }
    public Vector3 Direction { get; private set; }
    public bool DestinationReached { get; private set; }

    private float movementSpeed = 2f;

    private Vector3 origin;
    private Vector3 destination;
    private float currentTime;
    private float totalTime;

    public void Reset(Vector3 position) {
        Vector3 resetPosition = position;
        //resetPosition.y = 0f;
        transform.position = resetPosition;
        DestinationReached = true;
    }

    public void SetDestination(Vector3 destination) {
        DestinationReached = false;
        IsPaused = false;
        origin = transform.position;
        this.destination = destination;
        currentTime = 0f;

        float distance = Vector3.Distance(destination, transform.position);
        totalTime = distance / movementSpeed;

        Direction = Vector3.Normalize(destination - origin);
    }

    public void Pause() {
        IsPaused = true;
    }

    public void Resume() {
        IsPaused = false;
    }

    private void Update() {
        if (IsPaused || DestinationReached) return;

        currentTime += Time.deltaTime;
        float progression = currentTime / totalTime;

        float xDiff = destination.x - origin.x;
        float zDiff = destination.z - origin.z;

        float easedX =origin.x + EasingHelper.EaseInOutSine(progression) * xDiff;
        float easedZ = origin.z + EasingHelper.EaseInOutSine(progression) * zDiff;

        Vector3 newPosition = new Vector3(easedX, transform.position.y, easedZ);
        if(currentTime >= totalTime) {
            newPosition = destination;
            DestinationReached = true;
        }
        this.transform.position = newPosition;
    }
}
