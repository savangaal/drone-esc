﻿using UnityEngine;

public class AwakeState : ChildState {

    [SerializeField] private float wakeUpTime = 2f;
    [SerializeField] private Transform transformToMove;
    [SerializeField] private Transform placeToMoveTo;
    [SerializeField] private Transform placeToMoveFrom;
    [SerializeField] private AnimationClip wakeUpClip;
    [SerializeField] private AnimationClip idleClip;
    [SerializeField] private Animator childAnimator;
    [SerializeField] private GameObject[] thrusterEmitters;
    [SerializeField] private GameObject shockedFX;

    protected override void OnEnterState() {
        FindObjectOfType<ChildAnimation>().SetIdle(idleClip);
        FindObjectOfType<ChildAnimation>().Play(wakeUpClip);
        shockedFX.SetActive(true);
        shockedFX.GetComponent<ParticleSystem>().Play();
    }

    protected override void OnExitState() {
        childAnimator.enabled = true;

        // Turn on thruster particle effect
        foreach (GameObject thruster in thrusterEmitters) {
            thruster.SetActive(true);
        }
    }

    protected override void OnUpdate() {
        //Vector3 slerped = Vector3.Slerp(placeToMoveFrom.position, placeToMoveTo.position, timeInState / wakeUpTime);
        //transformToMove.position = slerped;
        if (timeInState > wakeUpTime) {
            subject.WakeUp();
        }
    }
}
