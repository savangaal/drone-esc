﻿using UnityEngine;
using System.Collections;
using System;

public class InteractionState : ChildState {

    private Vector3 droneStartPosition;
    private bool forwardLerp = true;
    private float timer = 0f;

    protected override void OnEnterState() {
        subject.WobbleAnimator.SetBool("StandStill",true);
        subject.MyRigidbody.isKinematic = true;
        forwardLerp = true;
        timer = 0f;
        droneStartPosition = subject.RotationContainer.transform.position;
        subject.ArmAnimation.OnEject += OnEject;
        subject.ArmAnimation.OnInject += OnInject;
        subject.ArmAnimation.OnUnlockComplete += OnUnlock;
        subject.ArmAnimation.OnAnimationComplete += OnCompleted;
        subject.ArmAnimation.PlayAnimation();
    }

    protected override void OnExitState() {
        subject.WobbleAnimator.SetBool("StandStill", false);
        subject.MyRigidbody.isKinematic = false;
        subject.ArmAnimation.OnEject -= OnEject;
        subject.ArmAnimation.OnInject -= OnInject;
        subject.ArmAnimation.OnUnlockComplete -= OnUnlock;
        subject.ArmAnimation.OnAnimationComplete -= OnCompleted;
    }

    protected override void OnUpdate() {
        timer += Time.deltaTime;
        Vector3 lerpedPosition = Vector3.Lerp(droneStartPosition, subject.QueuedInteraction.ChildInteractionPosition.position, timer);
        if (!forwardLerp) {
            lerpedPosition = Vector3.Lerp(subject.QueuedInteraction.ChildInteractionPosition.position, droneStartPosition, timer);
        }
        Vector3 direction = subject.RotationContainer.position - subject.QueuedInteraction.transform.position;
        subject.RotationContainer.transform.LookAt(subject.QueuedInteraction.ChildLookPosition, -direction);
        subject.RotationContainer.transform.position = lerpedPosition;

        if(timer > 1 && !forwardLerp) {
            subject.InteractionDone();
        }
    }

    private void OnUnlock() {
        subject.QueuedInteraction.OnInteractionDone();
        subject.QueuedInteraction.HandleInteraction();
    }

    private void OnInject() {
        subject.QueuedInteraction.OnInject();
    }

    private void OnEject() {
        
    }

    private void OnCompleted() {
        timer = 0f;
        forwardLerp = false;
        subject.QueuedInteraction.OnEject();
    }
}
