﻿using UnityEngine;
using System.Collections;

public abstract class ChildState : MonoBehaviour {

    protected ChildDrone subject { get; private set; }
    protected bool isActiveState { get; private set; }
    protected float timeInState { get; private set; }

    public void Initialize(ChildDrone subject) {
        this.subject = subject;
    }

    public void EnterState() {
        isActiveState = true;
        timeInState = 0.0f;
        OnEnterState();
    }

    public void ExitState() {
        isActiveState = false;
        OnExitState();
    }

    protected abstract void OnEnterState();
    protected abstract void OnExitState();
    protected abstract void OnUpdate();

    private void Update() {
        if (!isActiveState) { return; }
        timeInState += Time.deltaTime;
        OnUpdate();
    }

}