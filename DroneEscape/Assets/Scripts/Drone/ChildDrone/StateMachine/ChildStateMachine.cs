﻿using UnityEngine;
using System.Collections.Generic;

public class ChildStateMachine : MonoBehaviour {

    private ChildDrone subject;
    private List<ChildState> states = new List<ChildState>();
    private ChildState currentState;

    public void Initialize(ChildDrone subject) {
        this.subject = subject;
    }

    public void GotoState<U>(bool forceRestart = false) where U : ChildState {
        if (currentState != null) {
            if (typeof(U) == currentState.GetType() && !forceRestart) { return; }
            currentState.ExitState();
        }
        currentState = GetOrCreateState<U>();
        currentState.EnterState();
    }

    /// <summary> Check if the given type is the Child Drone's current state. </summary>
    public bool IsCurrentState<U>() where U : ChildState {
        if (currentState == null) { return false; }
        return currentState.GetType() == typeof(U);
    }

    private ChildState GetOrCreateState<U>() where U : ChildState {
        ChildState state = states.Find(x => x.GetType() == typeof(U));
        if (state == null) {
            state = subject.GetComponent<U>();
            if (state == null) {
                state = subject.gameObject.AddComponent<U>();
            }
            state.Initialize(subject);
            states.Add(state);
        }
        return state;
    }

    public string CurrentStateName() {
        return currentState.GetType().ToString();
    }

}

