﻿using UnityEngine;
using System.Collections;
using System;

public class MovingState : ChildState {

    protected override void OnEnterState() {
        subject.Walker.SetDestination(subject.Destination);
    }

    protected override void OnExitState() {
        subject.Walker.Reset(subject.transform.position);
    }

    protected override void OnUpdate() {
        Vector3 targetPosition = subject.Walker.transform.position;
        if (subject.Walker.DestinationReached && Vector3.Distance(targetPosition, transform.position) < 1.1) {
            subject.MovementDone();
        }
    }
}
