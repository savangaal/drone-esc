﻿using UnityEngine;
using System.Collections;
using System;

public class ShockState : ChildState {
    [SerializeField] private float stunTime = 1f;

    protected override void OnEnterState() {
        // activate stun mode on child
    }

    protected override void OnExitState() {
        // remove stun mode on child
    }

    protected override void OnUpdate() {
        if(timeInState > stunTime) {
            subject.ShockCompleted();
        }
    }
}
