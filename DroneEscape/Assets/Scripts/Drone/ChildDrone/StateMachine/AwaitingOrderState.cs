﻿using UnityEngine;
using System.Collections;
using System;

public class AwaitingOrderState : ChildState {

    protected override void OnEnterState() {
        subject.AwaitOrder();
        subject.Walker.Reset(subject.transform.position);
    }

    protected override void OnExitState() {

    }

    protected override void OnUpdate() {

    }
}
