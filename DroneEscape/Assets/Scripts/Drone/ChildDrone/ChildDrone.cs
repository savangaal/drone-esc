﻿using System;
using System.Collections;
using UnityEngine;

public class ChildDrone : MonoBehaviour {
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float smoothFactor;
    [SerializeField] private Transform rotationContainer;

    [SerializeField] private float maxDistanceToWalker;
    [SerializeField] private float maxSpeed;

    [SerializeField] private float force;

    [SerializeField] private ArmInteraction debugInteraction;

    [SerializeField] private Transform lightRoomDummySpawn;
    [SerializeField] private Transform darkRoomDummySpawn;

    [SerializeField] private float childMoveUpSpeed = 1f;
    [SerializeField] private Transform elevator;

    public Vector3 Destination { get; private set; }
    public ArmInteraction QueuedInteraction { get; private set;  }
    public ChildWalker Walker { get; private set; }
    public Rigidbody MyRigidbody { get; private set; }
    public ArmAnimationComponent ArmAnimation { get; private set; }
    public Transform RotationContainer { get { return rotationContainer; } }
    public Animator WobbleAnimator;
    public bool IsAwaitingOrder { get { return stateMachine.IsCurrentState<AwaitingOrderState>(); } }
    public bool IsMoving { get { return stateMachine.IsCurrentState<MovingState>(); } }

    private ChildStateMachine stateMachine;
    private bool isAwoken = false;
    private Transform childStartPosition;

    private void Awake() {
        Init();
    }

    private void Init() {
        stateMachine = gameObject.AddComponent<ChildStateMachine>();
        stateMachine.Initialize(this);
        stateMachine.GotoState<IdleState>();
        MyRigidbody = GetComponent<Rigidbody>();
        ArmAnimation = GetComponentInChildren<ArmAnimationComponent>();
        CreateWalker();
        GameState.SetCondition("ChildIsInDarkRoom", false);
        GameState.AddListener("ActTwoEnabled", OnActTwoEnabled);
        GameState.AddListener("ChildOnDoorButton", OnChildOnDoorButton);
        GameState.AddListener("PlayerStandsOnButton", OnPlayerStandsOnButton);
        GameState.AddListener("ElevatorTrigger", OnElevatorTriggered);
        childStartPosition = transform;
        gameObject.SetActive(false);
    }
    
    private void OnElevatorTriggered() {
        transform.parent = elevator;
        //FindObjectOfType<ChildAnimation>().enabled = false;
        //WobbleAnimator.enabled = false;
        stateMachine.GotoState<IdleState>();
    }

    private void OnActTwoEnabled() {
        Debug.Log("Act two enabled x child");
        gameObject.SetActive(true);
    }

    private void OnPlayerStandsOnButton() {
        // TODO Go to lift
    }

    private void OnChildOnDoorButton() {
        if (!GameState.CheckCondition("IsDoorButtonPressed")) {
            GoToOtherRoom();
        }
    }

    public void GoToOtherRoom() {
        if (!GameState.CheckCondition("ChildIsInDarkRoom")) {
            Walker.SetDestination(darkRoomDummySpawn.transform.position);
            GameState.SetCondition("ChildIsInDarkRoom", true);
        } else {
            Walker.SetDestination(lightRoomDummySpawn.transform.position);
            GameState.SetCondition("ChildIsInDarkRoom", false);
        }
    }

    public void PayAttention() {
        if (!isAwoken) return;
        if (stateMachine.IsCurrentState<InteractionState>()) { return; }
        stateMachine.GotoState<AwaitingOrderState>();
    }

    public void Shock() {
        if (!isAwoken) {
            stateMachine.GotoState<AwakeState>();
            return;
        }
        if (stateMachine.IsCurrentState<InteractionState>()) { return; }
        stateMachine.GotoState<ShockState>();
    }

    public void ShockCompleted() {
        stateMachine.GotoState<AwaitingOrderState>();
    }

    public void HandleInteraction(ArmInteraction interaction) {
        if (!stateMachine.IsCurrentState<AwaitingOrderState>()) { return; }

        QueuedInteraction = interaction;
        Destination = interaction.ChildInteractionPosition.position;
        stateMachine.GotoState<MovingState>();
    }

    public void InteractionDone() {
        QueuedInteraction = null;
        stateMachine.GotoState<IdleState>();
    }

    public void MoveTo(Vector3 point) {
        if (!stateMachine.IsCurrentState<AwaitingOrderState>()) { return; }

        Vector3 movePosition = point;
        movePosition.y = transform.position.y;
        Destination = movePosition;
        stateMachine.GotoState<MovingState>();
    }

    public void MovementDone() {
        if (QueuedInteraction != null) {
            stateMachine.GotoState<InteractionState>();
        } else {
            stateMachine.GotoState<IdleState>();
        }
    }
    
    public void AwaitOrder() {
        QueuedInteraction = null;
    }

    public void WakeUp() {
        isAwoken = true;
        stateMachine.GotoState<IdleState>();
    }

    private void Update() {
        // DEBUG
        DebugButtons();

        if (!isAwoken) return;

        MoveDrone();

        if (stateMachine.IsCurrentState<InteractionState>()) {
            LookAtPosition(QueuedInteraction.ChildLookPosition.position);
        } else if (stateMachine.IsCurrentState<AwaitingOrderState>()) {
            PlayerDrone player = FindObjectOfType<PlayerDrone>();
            LookAtPosition(player.transform.position);
        } else {
            LookAtFlyingDirection();
        }
    }

    private void DebugButtons() {
        if (Input.GetKeyDown(KeyCode.F12)){
            QueuedInteraction = debugInteraction;
            Destination = debugInteraction.ChildInteractionPosition.position;
            stateMachine.GotoState<MovingState>();
        }
        if (Input.GetKeyDown(KeyCode.F11)) {
            GoToOtherRoom();
        }
    }
    
    private void CreateWalker() {
        GameObject walkerGo = new GameObject("ChildWalker");
        Vector3 walkerPosition = transform.position;
        walkerPosition.y = 0;
        Walker = walkerGo.AddComponent<ChildWalker>();
        Walker.Reset(walkerPosition);
    }
    
    private void LookAtFlyingDirection() {
        Vector3 delta = Walker.transform.position - transform.position;
        Vector3 up = new Vector3(delta.x, 1.0f, delta.z).normalized;
        Vector3 newUp = Vector3.Lerp(up, rotationContainer.forward, smoothFactor);

        Quaternion rotationNoLook = Quaternion.LookRotation(newUp, rotationContainer.up);
        Quaternion rotationWithLook;
        rotationWithLook = Quaternion.LookRotation(newUp, Walker.Direction);

        rotationContainer.transform.rotation = Quaternion.RotateTowards(rotationNoLook, rotationWithLook, rotationSpeed * Time.fixedDeltaTime);
    }

    private void LookAtPosition(Vector3 position) {
        Vector3 delta = Walker.transform.position - transform.position;
        Vector3 up = new Vector3(delta.x, 1.0f, delta.z).normalized;
        Vector3 newUp = Vector3.Lerp(up, rotationContainer.forward, smoothFactor);

        Quaternion rotationNoLook = Quaternion.LookRotation(newUp, rotationContainer.up);
        
        Vector3 direction = position - transform.position;
        Quaternion rotationWithLook = Quaternion.LookRotation(newUp, direction);

        rotationContainer.transform.rotation = Quaternion.RotateTowards(rotationNoLook, rotationWithLook, rotationSpeed * Time.fixedDeltaTime);
    }

    private void MoveDrone() {
        float distanceToWalker = Vector3.Distance(transform.position, Walker.transform.position);

        if (distanceToWalker >= maxDistanceToWalker) {
            Walker.Pause();
        } else if (Walker.IsPaused) {
            Walker.Resume();
        }

        Vector3 targetPosition = Walker.transform.position;
        Vector3 deltaPosition = targetPosition - transform.position;
        if (MyRigidbody.velocity.magnitude < maxSpeed) {
            MyRigidbody.AddForce(deltaPosition * force);
        }
    }
}

