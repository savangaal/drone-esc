﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArmInteraction : MonoBehaviour {

    public bool InteractionValid = true;

    public Transform InteractionPosition;
    public Transform ChildLookPosition;
    public Transform ChildInteractionPosition;
    public Transform PlayerLookPosition;
    public string EventName;

    //public Material ColoredMaterial;

    private List<Material> materials = new List<Material>();

    public void HandleInteraction() {
        GameState.TriggerEvent(EventName);
    }

    public void OnInject() {

    }

    public void OnEject() {

    }

    public void OnInteractionDone() {

    }

    private void Awake() {
        /*
        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        foreach (Renderer render in renderers) {
            foreach(Material material in render.materials) {
                if(material.name.StartsWith(ColoredMaterial.name)) {
                    materials.Add(material);
                }
            }
        }
        */
    }
}
