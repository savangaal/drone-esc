﻿using UnityEngine;
using System.Collections;
using System;

public class FactoryResultBeamInteraction : GrabberInteraction {

    public override void HandleInteraction() {
        
        GameObject productObject = FindObjectOfType<Factory>().Collect();
        if (productObject == null) {
            Debug.LogError("No product in factory available");
            return;
        }
        ObjectToGrab = productObject.GetComponent<Rigidbody>();
    }
}
