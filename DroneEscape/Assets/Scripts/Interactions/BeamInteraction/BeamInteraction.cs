﻿using UnityEngine;
using System.Collections;

public abstract class GrabberInteraction : MonoBehaviour {

    public Rigidbody ObjectToGrab;
    public bool IsChargedMineral;

    public abstract void HandleInteraction();

}
