﻿using UnityEngine;
using System.Collections;
using System;

public class DispenserBeamInteraction : GrabberInteraction {

    public override void HandleInteraction() {
        Dispenser dispenser = GameObject.FindObjectOfType<Dispenser>();
        ObjectToGrab = dispenser.Claim();
    }
}
