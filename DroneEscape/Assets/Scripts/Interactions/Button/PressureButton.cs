﻿using UnityEngine;
using System.Collections;

public class PressureButton : MonoBehaviour {

    [SerializeField] private GameObject target;
    [SerializeField] private float duration;
    [SerializeField] private bool activate;

    private int collisionCount;
    private float timeSinceNoCollision;

    private void OnTriggerEnter() {
        collisionCount++;
        target.SetActive(activate);
    }

    private void OnTriggerExit() {
        collisionCount--;
        timeSinceNoCollision = 0.0f;
    }

    private void Update() {
        if (collisionCount > 0) { return; }
        if (timeSinceNoCollision >= duration) { return; }
        timeSinceNoCollision += Time.deltaTime;
        if (timeSinceNoCollision >= duration) {
            target.SetActive(!activate);
        }
    }

}
