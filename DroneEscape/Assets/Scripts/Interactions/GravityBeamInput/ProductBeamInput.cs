﻿using UnityEngine;
using System.Collections;
using System;

public class ProductBeamInput : GravityBeamInput {
    
    [SerializeField] private GameObject battery;

    private void Awake() {
        if (battery) {
            battery.SetActive(false);
        }
    }

    private void OnActTwoEnabled() {
        isFull = false;
        battery.SetActive(false);
    }

    protected override void OnTransitionComplete(Rigidbody body) {
            ProductInput productInput = FindObjectOfType<ProductInput>();
            productInput.Input(body.gameObject);
            battery.SetActive(true);
    }
}
