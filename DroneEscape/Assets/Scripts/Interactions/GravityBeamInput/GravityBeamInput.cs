﻿using UnityEngine;
using System.Collections;
using System;

public abstract class GravityBeamInput : MonoBehaviour {

    public bool AcceptsChargedMinerals;
    [SerializeField] private Transform inputPosition;
    public bool acceptsOnlyOne;
    public bool isFull;


    public virtual void Input(Rigidbody rigidbody) {
            StartCoroutine(LerpTransform(rigidbody));
            isFull = true;
    }

        private IEnumerator LerpTransform(Rigidbody body) {
        Transform transform = body.gameObject.transform;
        float time = 0f;
        Vector3 startPos = transform.position;
        while (time < 1) {
            time += Time.deltaTime;
            Vector3 lerpedVector = Vector3.Lerp(startPos, inputPosition.position, time);
            transform.position = lerpedVector;
            yield return new WaitForEndOfFrame();
        }
        OnTransitionComplete(body);
    }

    protected abstract void OnTransitionComplete(Rigidbody body);
}
