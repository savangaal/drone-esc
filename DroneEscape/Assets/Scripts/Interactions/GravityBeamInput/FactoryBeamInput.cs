﻿using UnityEngine;
using System.Collections;
using System;

public class FactoryBeamInput : GravityBeamInput {

    protected override void OnTransitionComplete(Rigidbody body) {
        Factory factory = GameObject.FindObjectOfType<Factory>();
        factory.Input(body);
    }
}
