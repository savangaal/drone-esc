﻿using UnityEngine;
using System.Collections;
using System;

public class FactoryAnimator : MonoBehaviour {

    public Action OnRefineComplete;

    [SerializeField] private GameObject materialGameObject;
    [SerializeField] private GameObject productGameObject;

    private Animator animator;

    public void PlaceMaterial() {
        productGameObject.SetActive(false);
        materialGameObject.SetActive(true);
    }

    public void GetProduct() {
        productGameObject.SetActive(false);
        animator.SetTrigger("Pickedup");
    }

    public void Refine() {
        animator.SetTrigger("StartRefine");
    }


    private void Animation_OnMaterialSwitch() {
        productGameObject.SetActive(true);
        materialGameObject.SetActive(false);
    }

    private void Animation_OnRefineComplete() {
        if (OnRefineComplete == null) return;
        OnRefineComplete();
    }

    private void Awake() {
        animator = GetComponent<Animator>();
        materialGameObject.SetActive(false);
        productGameObject.SetActive(false);
    }

}
