﻿using UnityEngine;
using System.Collections;
using System;

public class EntryShaft : MonoBehaviour {
    
    [SerializeField] private float closeDoorTime = 1f;

    [SerializeField] private Transform doorUp;
    [SerializeField] private Transform doorDown;
    [SerializeField] private Transform bladeLeft;
    [SerializeField] private Transform bladeRight;

    [SerializeField] private float openDoorYOffset;
    [SerializeField] private float openBladeXOffset;

	void Awake() {
        GameState.AddListener("ActTwoEnabled", OnActTwoEnabled);
        GameState.AddListener("PlayerLeftEntry", OnPlayerLeftEntry);
    }

    private void OnActTwoEnabled() {
        OpenBlades();
        OpenDoors();
    }

    private void OpenBlades() {
        Vector3 cachedPos = bladeLeft.position;
        cachedPos.x += openBladeXOffset;
        bladeLeft.position = cachedPos;

        cachedPos = bladeRight.position;
        cachedPos.x -= openBladeXOffset;
        bladeRight.position = cachedPos;
    }

    private void OpenDoors() {
        Vector3 cachedPos = doorUp.position;
        cachedPos.y += openDoorYOffset;
        doorUp.position = cachedPos;

        cachedPos = doorDown.position;
        cachedPos.y -= openDoorYOffset;
        doorDown.position = cachedPos;
    }

    private void OnPlayerLeftEntry() {
        StartCoroutine(CloseObject(doorUp, closeDoorTime, null));
        StartCoroutine(CloseObject(doorDown, closeDoorTime, null));
        StartCoroutine(CloseObject(bladeLeft, closeDoorTime, null));
        StartCoroutine(CloseObject(bladeRight, closeDoorTime, null));
    }

    private IEnumerator CloseObject(Transform door, float speed, Action onCompleted) {
        float timer = 0f;
        Vector3 doorStart = door.localPosition;

        while (timer < speed) {
            timer += Time.deltaTime;
            Vector3 slerpDoorPos = Vector3.Slerp(doorStart, Vector3.zero, timer / speed);
            door.localPosition = slerpDoorPos;
            yield return new WaitForEndOfFrame();
        }
        if (onCompleted != null) {
            onCompleted();
        }
    }
}
