﻿using System.Collections;
using UnityEngine;

public class ProductInput : MonoBehaviour {

    public static int InputtedAmount;
    public static int MaxCount { get; private set; }

    [SerializeField] private int startCount;
    [SerializeField] private int maxCount;
    [SerializeField] private string isFilledConditionName;
    [SerializeField] private string filledEventName;
    [SerializeField] private string resetEventName;


    public void Input(GameObject product) {
        Destroy(product);
        IncreaseProductCount();
    }

    private void Awake() {
        InputtedAmount = startCount;
        MaxCount = maxCount;
        GameState.SetCondition(isFilledConditionName, false);
        GameState.AddListener(resetEventName, OnResetEvent);
    }

    private void OnResetEvent() {
        InputtedAmount = 0;
    }

    private void Update() {
        if (Application.isEditor) {
            if (UnityEngine.Input.GetKeyDown(KeyCode.S)) {
                IncreaseProductCount();
            }
        }
    }

    private void IncreaseProductCount() {
        InputtedAmount++;
        if (InputtedAmount >= maxCount) {
            GameState.SetCondition(isFilledConditionName, true);
            GameState.TriggerEvent(filledEventName);
        }
    }

    

}
