﻿using UnityEngine;
using System.Collections;
using System;

public class Shaft : MonoBehaviour {
    
    [SerializeField] private float closeDoorTime = 1f;
    [SerializeField] private float closeBladesTime = 0.2f;

    [SerializeField] private float openDoorYOffset;
    [SerializeField] private float openBladeXOffset;

    [SerializeField] private Transform doorUp;
    [SerializeField] private Transform doorDown;
    [SerializeField] private Transform bladeLeft;
    [SerializeField] private Transform bladeRight;

    [SerializeField] private Collider doorCollider;

    [SerializeField] private Transform EndPosition;

    private void Awake() {
        GameState.AddListener("PlayerEnteredShaft", OnPlayerEnteredShaft);
        GameState.AddListener("ShaftActivated", OnPlayerActivatedShaft);
        OpenDoors();
        OpenBlades();
    }

    private void OpenBlades() {
        Vector3 cachedPos = bladeLeft.position;
        cachedPos.x += openBladeXOffset;
        bladeLeft.position = cachedPos;

        cachedPos = bladeRight.position;
        cachedPos.x -= openBladeXOffset;
        bladeRight.position = cachedPos;
    }

    private void OpenDoors() {
        Vector3 cachedPos = doorUp.position;
        cachedPos.y += openDoorYOffset;
        doorUp.position = cachedPos;

        cachedPos = doorDown.position;
        cachedPos.y -= openDoorYOffset;
        doorDown.position = cachedPos;

        doorCollider.enabled = false;
    }

    private void OnPlayerEnteredShaft() {
        doorCollider.enabled = true;
        StartCoroutine(CloseObject(doorUp,closeDoorTime, null));
        StartCoroutine(CloseObject(doorDown, closeDoorTime,null));
    }

    private void OnPlayerActivatedShaft() {
        StartCoroutine(CloseObject(bladeLeft, closeBladesTime, null));
        StartCoroutine(CloseObject(bladeRight, closeBladesTime, OnCutCompleted));
    }

    private void OnCutCompleted() {
        PlayerDrone player = FindObjectOfType<PlayerDrone>();
        player.BreakArm();
    }

    private IEnumerator CloseObject(Transform door, float speed, Action onCompleted) {
        float timer = 0f;
        Vector3 doorStart = door.localPosition;

        while(timer < speed) {
            timer += Time.deltaTime;
            Vector3 slerpDoorPos = Vector3.Slerp(doorStart, Vector3.zero, timer/speed);
            door.localPosition = slerpDoorPos;
            yield return new WaitForEndOfFrame();
        }
        if(onCompleted != null) {
            onCompleted();
        }
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.F3)) {
            PlayerDrone player = GameObject.FindObjectOfType<PlayerDrone>();
            OpenDoors();
            OpenBlades();
            player.transform.position = EndPosition.position;
        }
    }
}
