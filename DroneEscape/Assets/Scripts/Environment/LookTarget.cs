﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LookTarget : MonoBehaviour {

    public static LookTarget Find(string name) {
        LookTarget result = new List<LookTarget>(FindObjectsOfType<LookTarget>()).Find(x => x.name == name);
        if (result == null) {
            Debug.LogError("LookTarget '" + name + "' does not exist!");
        }
        return result;
    }

}
