﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager : MonoBehaviour {
    public GameObject GeneratorLights;
    public GameObject SecondaryLight;
    public GameObject DoorLight;
    public GameObject Button;
    public GameObject ElevatorLights;
    public GameObject DispenserLights;
    public GameObject EscapeShaftLights;

    public List<ReflectionProbe> ReflectionProbes = new List<ReflectionProbe>();

    public void LightSwitch(GameObject target){
        target.SetActive(!target.activeSelf);

        foreach (ReflectionProbe r in ReflectionProbes){
            r.RenderProbe();
        }
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            LightSwitch(ElevatorLights);
        }
        if (Input.GetKey(KeyCode.Alpha3))
        {
            LightSwitch(EscapeShaftLights);
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            LightSwitch(DispenserLights);
        }
    }

}
