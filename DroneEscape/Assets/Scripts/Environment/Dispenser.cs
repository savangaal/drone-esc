﻿using UnityEngine;
using System.Collections;
using System;

public class Dispenser : MonoBehaviour {

    [SerializeField] private Rigidbody mineralPrefab;
    [SerializeField] private Transform SpawnPosition;

    private Rigidbody spawnedMineral;

    public Rigidbody Claim() {
        StartCoroutine(DispenseOverTime());
        return spawnedMineral;
    }


    private void Awake() {
        Dispense();
    }

    private void Dispense() {
        spawnedMineral = Instantiate(mineralPrefab);
        spawnedMineral.gameObject.transform.position = SpawnPosition.transform.position;
    }

    private IEnumerator DispenseOverTime() {
        yield return new WaitForSeconds(1f);
        Dispense();
    }
}
