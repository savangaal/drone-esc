﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Stair : MonoBehaviour {

    [SerializeField] private Transform stairCollider;
    [SerializeField] private Transform blockCollider;
    [SerializeField] private Transform freedomStairCollider;
    [SerializeField] private GameObject stepPrefab;
    
    [SerializeField] private Transform startLocator;
    [SerializeField] private Transform endLocator;

    [SerializeField] private Transform freedomStartLocator;
    [SerializeField] private Transform freedomEndLocator;

    [SerializeField] private int stepAmount;
    [SerializeField] private float transitionDuration;
    [SerializeField] private float freedomTransitionDuration;
    [SerializeField] private string enabledConditionName;
    [SerializeField] private string turnEventName;
    [SerializeField] private float enableDuration;

    private readonly List<GameObject> steps = new List<GameObject>();

    private float timeSinceConditionTrue;
    private bool isEnabled;
    private bool isTurned;

    private void Awake() {
        ConstructSteps();
        stairCollider.transform.position = startLocator.position;
        stairCollider.transform.forward = endLocator.position - startLocator.position;

        GameState.AddListener(turnEventName, OnTurnEvent);
        freedomStairCollider.gameObject.SetActive(false);
    }

    private void Update() {
        UpdateFirstStairColliders();
        UpdateStepsVisible();

        if (!isTurned) {
            UpdateStairEnabled();
        }

        if (Application.isEditor) {
            if (Input.GetKeyDown(KeyCode.B)) {
                timeSinceConditionTrue = 0.0f;
                SetStairEnabled(true);
            }
        }
    }

    private void OnTurnEvent() {
        if (isTurned) { return; }
        isTurned = true;
        StopAllCoroutines();
        StartCoroutine("UpdateStepsToTurnedPositionOverTime");
        freedomStairCollider.gameObject.SetActive(true);
    }

    private void UpdateFirstStairColliders() {
        if (isTurned) {
            stairCollider.gameObject.SetActive(false);
            blockCollider.gameObject.SetActive(true);
            blockCollider.transform.position = startLocator.position;
            return;
        }
        stairCollider.gameObject.SetActive(true);
        
        Vector3 scale = stairCollider.transform.localScale;
        float stairCompletion = (float)ProductInput.InputtedAmount / (float)ProductInput.MaxCount;
        float distance = Vector3.Distance(startLocator.position, endLocator.position);
        scale.z = stairCompletion * distance;

        stairCollider.gameObject.SetActive(isEnabled);
        stairCollider.transform.localScale = scale;

        blockCollider.gameObject.SetActive(stairCompletion < 1.0f && isEnabled);
        blockCollider.transform.position = Vector3.Lerp(startLocator.transform.position, endLocator.transform.position, stairCompletion);
    }

    private void UpdateStepsVisible() {
        for (int i = 0; i < steps.Count; i++) {
            //steps[i].SetActive(i < ProductInput.InputtedAmount);
            steps[i].SetActive(ProductInput.InputtedAmount >= ProductInput.MaxCount);
        }
    }

    private void UpdateStairEnabled() {
        if (GameState.CheckCondition(enabledConditionName)) {
            timeSinceConditionTrue = 0.0f;
            SetStairEnabled(true);
        }

        if (timeSinceConditionTrue < enableDuration) {
            timeSinceConditionTrue += Time.deltaTime;
            if (timeSinceConditionTrue >= enableDuration) {
                SetStairEnabled(false);
            }
        }
    }

    private void ConstructSteps() {
        Vector3 direction = endLocator.position - startLocator.position;
        direction.y = 0.0f;

        for (int i = 0; i < stepAmount; i++) {
            GameObject step = Instantiate(stepPrefab);
            step.transform.SetParent(transform);

            Vector3 position = Vector3.Lerp(startLocator.position, endLocator.position, (float)i / (float)(stepAmount - 1));
            position.y = startLocator.position.y;
            step.transform.position = position;
            step.transform.forward = direction;

            steps.Add(step);
        }
    }

    private void SetStairEnabled(bool enable) {
        if (isEnabled == enable) { return; }
        isEnabled = enable;
        StopCoroutine("MoveStepsOverTime");
        StartCoroutine("MoveStepsOverTime", enable);
    }

    private IEnumerator MoveStepsOverTime(bool enable) {

        float duration = 0.0f;

        List<Vector3> startPositions = new List<Vector3>();
        steps.ForEach(x => startPositions.Add(x.transform.position));

        Action<float> updatePositions = delegate(float timeLerp) {
            for (int i = 0; i < steps.Count; i++) {

                Vector3 start = startPositions[i];
                Vector3 end;

                if (enable) {
                    float stepLerp = (float)i / (float)(steps.Count - 1);
                    end = new Vector3(start.x, Mathf.Lerp(startLocator.position.y, endLocator.position.y, stepLerp), start.z);
                } else {
                    end = new Vector3(start.x, startLocator.position.y, start.z);
                }

                float easedLerp = EasingHelper.EaseInOutSine(timeLerp);
                Vector3 position = Vector3.Lerp(start, end, easedLerp);

                steps[i].transform.position = new Vector3(steps[i].transform.position.x, position.y, steps[i].transform.position.z);
            }
        };

        while (duration < transitionDuration){
            duration += Time.deltaTime;
            updatePositions(duration / transitionDuration);
            yield return new WaitForEndOfFrame();
        }

        updatePositions(1.0f);
    }

    private IEnumerator UpdateStepsToTurnedPositionOverTime() {
        float duration = 0.0f;

        List<Vector3> startPositions = new List<Vector3>();
        steps.ForEach(x => startPositions.Add(x.transform.position));

        Vector3 startForward = steps[0].transform.forward;
        Vector3 endForward = freedomEndLocator.position - freedomStartLocator.position;
        endForward.y = 0.0f;

        Action<float> updatePositions = delegate(float timeLerp) {
            for (int i = 0; i < steps.Count; i++) {

                Vector3 start = startPositions[i];

                float stepLerp = (float)i / (float)(steps.Count - 1);
                Vector3 end = Vector3.Lerp(freedomStartLocator.position, freedomEndLocator.position, stepLerp);

                float easedLerp = EasingHelper.EaseInOutSine(timeLerp);
                Vector3 position = Vector3.Lerp(start, end, easedLerp);

                steps[i].transform.position = position;
                steps[i].transform.forward = Vector3.Lerp(startForward, endForward, easedLerp);
            }
        };

        while (duration < freedomTransitionDuration) {
            duration += Time.deltaTime;
            updatePositions(duration / freedomTransitionDuration);
            yield return new WaitForEndOfFrame();
        }

        updatePositions(1.0f);
    }

}
