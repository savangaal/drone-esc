﻿using UnityEngine;
using System.Collections;
using System;

public class Factory : MonoBehaviour {

    private enum State {
        Empty,
        Filled,
        Processing,
        Result
    }

    [SerializeField] private string interactEvent;

    [SerializeField] private string onMineralEnterEvent;
    [SerializeField] private string onProcessingEvent;
    [SerializeField] private string onNoMineralEvent;
	[SerializeField] private string onDoneEvent;
    [SerializeField] private string onCollectEvent;
    [SerializeField] private string hasOutputCondition;

    [SerializeField] private Transform inputLocator;
    [SerializeField] private Transform outputLocator;

    [SerializeField] private GameObject productPrefab;
    [SerializeField] private string hasMineralConditionName;

    [SerializeField] private Transform capsulePosition;

    private State state;
    private FactoryAnimator factoryAnimator;

    public void Input(Rigidbody mineral) {
        GameObject go = mineral.gameObject;
        Destroy(mineral);
        StartCoroutine(MoveToInputLocation(go));
        if (state == State.Empty) {
            GameState.SetCondition(hasMineralConditionName, true);
            state = State.Filled;
            GameState.TriggerEvent(onMineralEnterEvent);
        }
    }

    public GameObject Collect() {
        GameObject result = Instantiate(productPrefab);
        result.transform.position = capsulePosition.position;
        result.transform.rotation = capsulePosition.rotation;
        state = State.Empty;
        factoryAnimator.GetProduct();
        GameState.TriggerEvent(onCollectEvent);
        GameState.SetCondition(hasMineralConditionName, false);
        GameState.SetCondition(hasOutputCondition, false);
        return result;
    }

    private void Awake() {
        GameState.AddListener(interactEvent, OnInteract);
        factoryAnimator = GetComponentInChildren<FactoryAnimator>();
        factoryAnimator.OnRefineComplete += OnRefineCompleted;
    }

    private void OnDestroy() {
        factoryAnimator.OnRefineComplete -= OnRefineCompleted;
    }

    private void OnRefineCompleted() {
        if (state == State.Processing) {
            Deliver();
        }
    }

    private void OnInteract() {
        if (state == State.Filled) {
            state = State.Processing;
            factoryAnimator.Refine();
            GameState.TriggerEvent(onProcessingEvent);
        } else {
            GameState.TriggerEvent(onNoMineralEvent);
        }
    }

    private void Deliver() {
        state = State.Result;
        GameState.TriggerEvent(onDoneEvent);
        GameState.SetCondition(hasOutputCondition, true);
    }

    private IEnumerator MoveToInputLocation(GameObject go) {

        Vector3 startPos = go.transform.position;
        Quaternion startRot = go.transform.rotation;
        float timer = 0;

        while (timer < 1) {
            timer += Time.deltaTime;
            Vector3 slerpedPos = Vector3.Slerp(startPos, capsulePosition.transform.position, timer);
            Quaternion slerpedRot = Quaternion.Slerp(startRot, capsulePosition.transform.rotation, timer);

            go.transform.position = slerpedPos;
            go.transform.rotation = slerpedRot;
            yield return new WaitForEndOfFrame();
        }
        factoryAnimator.PlaceMaterial();
        Destroy(go);
    }

}