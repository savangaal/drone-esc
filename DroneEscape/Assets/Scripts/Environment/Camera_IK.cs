﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class Camera_IK : MonoBehaviour {
    public Transform hinge;
    public Transform aim;
    public Transform aim1;
    public Transform cam;
    public Transform target;
    float t = 2f;
    Quaternion oldrot;

    void Awake () {
        if(GameObject.Find("PlayerDrone")) target = GameObject.Find("PlayerDrone").transform;
	}
	
	void LateUpdate () {


        if (target)
        {
            Vector3 camlookAtPos = target.position;
            Vector3 camlookAtPosLocalised = hinge.InverseTransformPoint(target.position);

            camlookAtPosLocalised.y = 0f;
            camlookAtPos = hinge.TransformPoint(camlookAtPosLocalised);

            Vector3 lookAtPos = target.position;
            Vector3 lookAtPosLocalised = hinge.InverseTransformPoint(target.position);

            lookAtPosLocalised.x = 0f;
            lookAtPos = hinge.TransformPoint(lookAtPosLocalised);

            t -= Time.deltaTime;

            if (t <= 0)
            {
                aim.LookAt(lookAtPos);
                aim1.LookAt(camlookAtPos);
                hinge.rotation = Quaternion.Lerp(hinge.rotation, aim.rotation, 3f * Time.deltaTime);
                cam.rotation = Quaternion.Lerp(cam.rotation, aim1.rotation, 3f * Time.deltaTime);
                StartCoroutine("Look");
            }

        }
    }

    IEnumerator Look ()
    {
        yield return new WaitForSeconds(Random.Range(2, 4));
        t = Random.Range(3, 8f);
    }

}
