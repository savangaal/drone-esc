﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightswitch : MonoBehaviour {
    public GameObject target;
    LightManager lightmanager;

    private void Awake()
    {
        lightmanager.LightSwitch(target);

        foreach (ReflectionProbe r in lightmanager.ReflectionProbes)
        {
            r.RenderProbe();
        }
    }
}
