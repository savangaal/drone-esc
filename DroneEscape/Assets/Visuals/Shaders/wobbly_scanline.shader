// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:33094,y:32700,varname:node_3138,prsc:2|emission-5985-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32445,y:32521,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Tex2d,id:3559,x:32445,y:32704,ptovrint:False,ptlb:node_3559,ptin:_node_3559,varname:node_3559,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:6be937bcf0622f64fb657621346198b6,ntxv:1,isnm:False|UVIN-7839-UVOUT,MIP-273-X;n:type:ShaderForge.SFN_FragmentPosition,id:273,x:32237,y:32854,varname:node_273,prsc:2;n:type:ShaderForge.SFN_Panner,id:7839,x:32237,y:32704,varname:node_7839,prsc:2,spu:2,spv:0|UVIN-3417-UVOUT,DIST-5217-TSL;n:type:ShaderForge.SFN_Time,id:5217,x:31999,y:32878,varname:node_5217,prsc:2;n:type:ShaderForge.SFN_VertexColor,id:3577,x:32237,y:33136,varname:node_3577,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5985,x:32762,y:32755,varname:node_5985,prsc:2|A-7241-RGB,B-3559-RGB,C-2966-RGB,D-9757-OUT;n:type:ShaderForge.SFN_TexCoord,id:3417,x:31999,y:32722,varname:node_3417,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:2966,x:32445,y:32892,ptovrint:False,ptlb:node_3559_copy,ptin:_node_3559_copy,varname:_node_3559_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:6be937bcf0622f64fb657621346198b6,ntxv:1,isnm:False|UVIN-1747-UVOUT,MIP-273-X;n:type:ShaderForge.SFN_Panner,id:1747,x:32237,y:32990,varname:node_1747,prsc:2,spu:-2,spv:0|UVIN-3417-UVOUT,DIST-5087-OUT;n:type:ShaderForge.SFN_Multiply,id:5087,x:31999,y:33042,varname:node_5087,prsc:2|A-5217-TSL,B-9356-OUT;n:type:ShaderForge.SFN_Vector1,id:9356,x:31822,y:33063,varname:node_9356,prsc:2,v1:0.15;n:type:ShaderForge.SFN_Add,id:5988,x:32237,y:33257,varname:node_5988,prsc:2|A-3417-V,B-9586-OUT;n:type:ShaderForge.SFN_Multiply,id:9757,x:32488,y:33082,varname:node_9757,prsc:2|A-3577-R,B-1857-OUT;n:type:ShaderForge.SFN_Vector1,id:9586,x:32050,y:33337,varname:node_9586,prsc:2,v1:0;n:type:ShaderForge.SFN_OneMinus,id:1857,x:32397,y:33257,varname:node_1857,prsc:2|IN-5988-OUT;proporder:7241-3559-2966;pass:END;sub:END;*/

Shader "Craft/wobbly_scanline" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _node_3559 ("node_3559", 2D) = "gray" {}
        _node_3559_copy ("node_3559_copy", 2D) = "gray" {}
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _node_3559; uniform float4 _node_3559_ST;
            uniform sampler2D _node_3559_copy; uniform float4 _node_3559_copy_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 node_5217 = _Time + _TimeEditor;
                float2 node_7839 = (i.uv0+node_5217.r*float2(2,0));
                float4 _node_3559_var = tex2Dlod(_node_3559,float4(TRANSFORM_TEX(node_7839, _node_3559),0.0,i.posWorld.r));
                float node_9356 = 0.15;
                float2 node_1747 = (i.uv0+(node_5217.r*node_9356)*float2(-2,0));
                float4 _node_3559_copy_var = tex2Dlod(_node_3559_copy,float4(TRANSFORM_TEX(node_1747, _node_3559_copy),0.0,i.posWorld.r));
                float3 emissive = (_Color.rgb*_node_3559_var.rgb*_node_3559_copy_var.rgb*(i.vertexColor.r*(1.0 - (i.uv0.g+0.0))));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
