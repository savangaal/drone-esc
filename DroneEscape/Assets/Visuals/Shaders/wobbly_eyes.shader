// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.29 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.29;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32537,y:32711,varname:node_3138,prsc:2|emission-3100-OUT;n:type:ShaderForge.SFN_Tex2d,id:9989,x:31743,y:32726,ptovrint:False,ptlb:FrontTexture,ptin:_FrontTexture,varname:node_9989,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:7930f0e34c2a7864caed1d0c8d454cfd,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:6205,x:31743,y:32920,ptovrint:False,ptlb:BackTexture,ptin:_BackTexture,varname:node_6205,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a8ac92095221fc64984198910110e2ff,ntxv:3,isnm:False;n:type:ShaderForge.SFN_Lerp,id:4601,x:31984,y:32840,varname:node_4601,prsc:2|A-9989-RGB,B-6205-RGB,T-1798-OUT;n:type:ShaderForge.SFN_Slider,id:1798,x:31586,y:33118,ptovrint:False,ptlb:LerpFactor,ptin:_LerpFactor,varname:node_1798,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Time,id:1445,x:31205,y:32463,varname:node_1445,prsc:2;n:type:ShaderForge.SFN_Sin,id:5404,x:31616,y:32535,varname:node_5404,prsc:2|IN-9812-OUT;n:type:ShaderForge.SFN_Vector1,id:7188,x:31443,y:32430,varname:node_7188,prsc:2,v1:0.9;n:type:ShaderForge.SFN_Vector1,id:8533,x:31443,y:32370,varname:node_8533,prsc:2,v1:1;n:type:ShaderForge.SFN_Subtract,id:8718,x:31616,y:32306,varname:node_8718,prsc:2|A-8533-OUT,B-7188-OUT;n:type:ShaderForge.SFN_Multiply,id:8276,x:31792,y:32535,varname:node_8276,prsc:2|A-8718-OUT,B-5404-OUT;n:type:ShaderForge.SFN_Add,id:5864,x:31985,y:32556,varname:node_5864,prsc:2|A-8276-OUT,B-7188-OUT;n:type:ShaderForge.SFN_Multiply,id:3100,x:32319,y:32813,varname:node_3100,prsc:2|A-5864-OUT,B-4601-OUT;n:type:ShaderForge.SFN_Pi,id:2944,x:31238,y:32592,varname:node_2944,prsc:2;n:type:ShaderForge.SFN_Multiply,id:9812,x:31409,y:32535,varname:node_9812,prsc:2|A-1445-TTR,B-2944-OUT;proporder:9989-6205-1798;pass:END;sub:END;*/

Shader "Craft/wobbly_eyes" {
    Properties {
        _FrontTexture ("FrontTexture", 2D) = "white" {}
        _BackTexture ("BackTexture", 2D) = "bump" {}
        _LerpFactor ("LerpFactor", Range(0, 1)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _FrontTexture; uniform float4 _FrontTexture_ST;
            uniform sampler2D _BackTexture; uniform float4 _BackTexture_ST;
            uniform float _LerpFactor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float node_7188 = 0.9;
                float4 node_1445 = _Time + _TimeEditor;
                float4 _FrontTexture_var = tex2D(_FrontTexture,TRANSFORM_TEX(i.uv0, _FrontTexture));
                float4 _BackTexture_var = tex2D(_BackTexture,TRANSFORM_TEX(i.uv0, _BackTexture));
                float3 emissive = ((((1.0-node_7188)*sin((node_1445.a*3.141592654)))+node_7188)*lerp(_FrontTexture_var.rgb,_BackTexture_var.rgb,_LerpFactor));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
