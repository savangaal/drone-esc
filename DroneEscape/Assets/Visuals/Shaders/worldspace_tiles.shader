// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:1,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:True,rpth:1,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:34742,y:32637,varname:node_2865,prsc:2|diff-3846-OUT,spec-358-OUT,gloss-4889-OUT,normal-946-OUT;n:type:ShaderForge.SFN_Slider,id:358,x:33725,y:32624,ptovrint:False,ptlb:Metallic,ptin:_Metallic,varname:_Metallic,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_FragmentPosition,id:1998,x:30258,y:32566,varname:node_1998,prsc:2;n:type:ShaderForge.SFN_Append,id:2899,x:30636,y:32510,varname:node_2899,prsc:2|A-1998-Y,B-1998-Z;n:type:ShaderForge.SFN_Append,id:490,x:30636,y:32670,varname:node_490,prsc:2|A-1998-Z,B-1998-X;n:type:ShaderForge.SFN_Append,id:7564,x:30636,y:32837,varname:node_7564,prsc:2|A-1998-X,B-1998-Y;n:type:ShaderForge.SFN_Tex2dAsset,id:1209,x:31189,y:31730,ptovrint:False,ptlb:Albedo,ptin:_Albedo,varname:_Albedo,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:086b7089379d76a408496e1e49c539ef,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:3980,x:31912,y:32231,varname:node_3980,prsc:2,tex:086b7089379d76a408496e1e49c539ef,ntxv:0,isnm:False|UVIN-3856-UVOUT,TEX-1209-TEX;n:type:ShaderForge.SFN_Tex2d,id:7309,x:31912,y:32429,varname:node_7309,prsc:2,tex:086b7089379d76a408496e1e49c539ef,ntxv:0,isnm:False|UVIN-8852-UVOUT,TEX-1209-TEX;n:type:ShaderForge.SFN_Tex2d,id:5659,x:31912,y:32635,varname:node_5659,prsc:2,tex:086b7089379d76a408496e1e49c539ef,ntxv:0,isnm:False|UVIN-3376-UVOUT,TEX-1209-TEX;n:type:ShaderForge.SFN_ChannelBlend,id:6476,x:32210,y:32399,varname:node_6476,prsc:2,chbt:0|M-2868-OUT,R-3980-RGB,G-7309-RGB,B-5659-RGB;n:type:ShaderForge.SFN_NormalVector,id:3184,x:30613,y:32104,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:2868,x:30976,y:32199,varname:node_2868,prsc:2|A-3184-OUT,B-3184-OUT;n:type:ShaderForge.SFN_ChannelBlend,id:946,x:32836,y:32966,varname:node_946,prsc:2,chbt:0|M-2868-OUT,R-8480-OUT,G-5123-OUT,B-1611-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:9221,x:31244,y:33175,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:_Normal,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:086b7089379d76a408496e1e49c539ef,ntxv:1,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:733,x:31914,y:32835,varname:node_733,prsc:2,tex:086b7089379d76a408496e1e49c539ef,ntxv:0,isnm:False|UVIN-3856-UVOUT,TEX-9221-TEX;n:type:ShaderForge.SFN_Tex2d,id:7787,x:31913,y:33046,varname:node_7787,prsc:2,tex:086b7089379d76a408496e1e49c539ef,ntxv:0,isnm:False|UVIN-8852-UVOUT,TEX-9221-TEX;n:type:ShaderForge.SFN_Tex2d,id:4520,x:31913,y:33244,varname:node_4520,prsc:2,tex:086b7089379d76a408496e1e49c539ef,ntxv:0,isnm:False|UVIN-3376-UVOUT,TEX-9221-TEX;n:type:ShaderForge.SFN_ChannelBlend,id:5467,x:32836,y:33158,varname:node_5467,prsc:2,chbt:0|M-2868-OUT,R-733-A,G-7787-A,B-4520-A;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:4889,x:33857,y:33468,varname:node_4889,prsc:2|IN-5467-OUT,IMIN-6804-OUT,IMAX-1562-OUT,OMIN-9399-OUT,OMAX-9333-OUT;n:type:ShaderForge.SFN_Slider,id:8464,x:32882,y:33644,ptovrint:False,ptlb:min roughness,ptin:_minroughness,varname:_minroughness,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Vector1,id:6804,x:33410,y:33428,varname:node_6804,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:1562,x:33410,y:33531,varname:node_1562,prsc:2,v1:1;n:type:ShaderForge.SFN_Slider,id:9333,x:32882,y:33868,ptovrint:False,ptlb:maxroughness,ptin:_maxroughness,varname:_maxroughness,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Blend,id:521,x:32850,y:32396,varname:node_521,prsc:2,blmd:10,clmp:True|SRC-6476-OUT,DST-9254-RGB;n:type:ShaderForge.SFN_Color,id:9254,x:32599,y:32542,ptovrint:False,ptlb:node_9254,ptin:_node_9254,varname:_node_9254,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Vector1,id:8178,x:30980,y:33057,varname:node_8178,prsc:2,v1:1.5708;n:type:ShaderForge.SFN_Slider,id:4814,x:30518,y:33125,ptovrint:False,ptlb:tiling,ptin:_tiling,varname:_tiling,prsc:0,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:10;n:type:ShaderForge.SFN_UVTile,id:3856,x:30980,y:32519,varname:node_3856,prsc:2|UVIN-2899-OUT,WDT-4814-OUT,HGT-4814-OUT,TILE-8164-OUT;n:type:ShaderForge.SFN_UVTile,id:8852,x:30980,y:32681,varname:node_8852,prsc:2|UVIN-490-OUT,WDT-4814-OUT,HGT-4814-OUT,TILE-8164-OUT;n:type:ShaderForge.SFN_UVTile,id:3376,x:30980,y:32850,varname:node_3376,prsc:2|UVIN-442-UVOUT,WDT-4814-OUT,HGT-4814-OUT,TILE-8164-OUT;n:type:ShaderForge.SFN_Slider,id:8164,x:30518,y:33315,ptovrint:False,ptlb:offset,ptin:_offset,varname:_offset,prsc:0,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:10;n:type:ShaderForge.SFN_Tex2d,id:9653,x:31920,y:31685,varname:_node_158,prsc:2,tex:086b7089379d76a408496e1e49c539ef,ntxv:0,isnm:False|UVIN-8852-UVOUT,TEX-1209-TEX;n:type:ShaderForge.SFN_Blend,id:3846,x:33447,y:32048,varname:node_3846,prsc:2,blmd:1,clmp:True|SRC-521-OUT,DST-4093-OUT;n:type:ShaderForge.SFN_Blend,id:4093,x:33249,y:31848,varname:node_4093,prsc:2,blmd:10,clmp:True|SRC-3810-OUT,DST-7838-OUT;n:type:ShaderForge.SFN_Slider,id:7838,x:32894,y:31953,ptovrint:False,ptlb:AO Intensity,ptin:_AOIntensity,varname:_AOIntensity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Tex2d,id:7315,x:31924,y:31414,varname:_node_1733,prsc:2,tex:086b7089379d76a408496e1e49c539ef,ntxv:0,isnm:False|UVIN-3856-UVOUT,TEX-1209-TEX;n:type:ShaderForge.SFN_Tex2d,id:8567,x:31920,y:31936,varname:_node_6794,prsc:2,tex:086b7089379d76a408496e1e49c539ef,ntxv:0,isnm:False|UVIN-3376-UVOUT,TEX-1209-TEX;n:type:ShaderForge.SFN_ChannelBlend,id:3810,x:32209,y:31722,varname:node_3810,prsc:2,chbt:0|M-2868-OUT,R-7315-A,G-9653-A,B-8567-A;n:type:ShaderForge.SFN_Fresnel,id:5548,x:32529,y:33398,varname:node_5548,prsc:2|NRM-3184-OUT;n:type:ShaderForge.SFN_Multiply,id:4527,x:33002,y:33454,varname:node_4527,prsc:2|A-5548-OUT,B-7493-OUT;n:type:ShaderForge.SFN_Slider,id:7493,x:32417,y:33604,ptovrint:False,ptlb:fresnel intensity,ptin:_fresnelintensity,varname:_fresnelintensity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Blend,id:9399,x:33278,y:33600,varname:node_9399,prsc:2,blmd:19,clmp:True|SRC-4527-OUT,DST-8464-OUT;n:type:ShaderForge.SFN_Rotator,id:442,x:30791,y:32860,varname:node_442,prsc:2|UVIN-7564-OUT,ANG-9676-OUT;n:type:ShaderForge.SFN_Vector1,id:9676,x:30610,y:33036,varname:node_9676,prsc:2,v1:1.5708;n:type:ShaderForge.SFN_RemapRange,id:8480,x:32308,y:32803,varname:node_8480,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-733-RGB;n:type:ShaderForge.SFN_RemapRange,id:5123,x:32309,y:33007,varname:node_5123,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-7787-RGB;n:type:ShaderForge.SFN_RemapRange,id:1611,x:32309,y:33203,varname:node_1611,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-4520-RGB;proporder:358-9221-1209-8464-9333-9254-4814-8164-7838-7493;pass:END;sub:END;*/

Shader "Shader Forge/worldspace_tiles" {
    Properties {
        _Metallic ("Metallic", Range(0, 1)) = 1
        _Normal ("Normal", 2D) = "gray" {}
        _Albedo ("Albedo", 2D) = "white" {}
        _minroughness ("min roughness", Range(0, 1)) = 0
        _maxroughness ("maxroughness", Range(0, 1)) = 0
        _node_9254 ("node_9254", Color) = (0.5,0.5,0.5,1)
        _tiling ("tiling", Range(0, 10)) = 0
        _offset ("offset", Range(0, 10)) = 0
        _AOIntensity ("AO Intensity", Range(0, 1)) = 1
        _fresnelintensity ("fresnel intensity", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "DEFERRED"
            Tags {
                "LightMode"="Deferred"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_DEFERRED
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile ___ UNITY_HDR_ON
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Metallic;
            uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _minroughness;
            uniform float _maxroughness;
            uniform float4 _node_9254;
            uniform fixed _tiling;
            uniform fixed _offset;
            uniform float _AOIntensity;
            uniform float _fresnelintensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float3 tangentDir : TEXCOORD4;
                float3 bitangentDir : TEXCOORD5;
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD6;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            void frag(
                VertexOutput i,
                out half4 outDiffuse : SV_Target0,
                out half4 outSpecSmoothness : SV_Target1,
                out half4 outNormal : SV_Target2,
                out half4 outEmission : SV_Target3 )
            {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 node_2868 = (i.normalDir*i.normalDir);
                float2 node_3856_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_3856_ty = floor(_offset * node_3856_tc_rcp.x);
                float node_3856_tx = _offset - _tiling * node_3856_ty;
                float2 node_3856 = (float2(i.posWorld.g,i.posWorld.b) + float2(node_3856_tx, node_3856_ty)) * node_3856_tc_rcp;
                float4 node_733 = tex2D(_Normal,TRANSFORM_TEX(node_3856, _Normal));
                float2 node_8852_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_8852_ty = floor(_offset * node_8852_tc_rcp.x);
                float node_8852_tx = _offset - _tiling * node_8852_ty;
                float2 node_8852 = (float2(i.posWorld.b,i.posWorld.r) + float2(node_8852_tx, node_8852_ty)) * node_8852_tc_rcp;
                float4 node_7787 = tex2D(_Normal,TRANSFORM_TEX(node_8852, _Normal));
                float2 node_3376_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_3376_ty = floor(_offset * node_3376_tc_rcp.x);
                float node_3376_tx = _offset - _tiling * node_3376_ty;
                float node_442_ang = 1.5708;
                float node_442_spd = 1.0;
                float node_442_cos = cos(node_442_spd*node_442_ang);
                float node_442_sin = sin(node_442_spd*node_442_ang);
                float2 node_442_piv = float2(0.5,0.5);
                float2 node_442 = (mul(float2(i.posWorld.r,i.posWorld.g)-node_442_piv,float2x2( node_442_cos, -node_442_sin, node_442_sin, node_442_cos))+node_442_piv);
                float2 node_3376 = (node_442 + float2(node_3376_tx, node_3376_ty)) * node_3376_tc_rcp;
                float4 node_4520 = tex2D(_Normal,TRANSFORM_TEX(node_3376, _Normal));
                float3 normalLocal = (node_2868.r*(node_733.rgb*2.0+-1.0) + node_2868.g*(node_7787.rgb*2.0+-1.0) + node_2868.b*(node_4520.rgb*2.0+-1.0));
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float node_6804 = 0.0;
                float node_9399 = saturate((_minroughness-((1.0-max(0,dot(i.normalDir, viewDirection)))*_fresnelintensity)));
                float gloss = 1.0 - (node_9399 + ( ((node_2868.r*node_733.a + node_2868.g*node_7787.a + node_2868.b*node_4520.a) - node_6804) * (_maxroughness - node_9399) ) / (1.0 - node_6804)); // Convert roughness to gloss
                float perceptualRoughness = (node_9399 + ( ((node_2868.r*node_733.a + node_2868.g*node_7787.a + node_2868.b*node_4520.a) - node_6804) * (_maxroughness - node_9399) ) / (1.0 - node_6804));
                float roughness = perceptualRoughness * perceptualRoughness;
/////// GI Data:
                UnityLight light; // Dummy light
                light.color = 0;
                light.dir = half3(0,1,0);
                light.ndotl = max(0,dot(normalDirection,light.dir));
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = 1;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
////// Specular:
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float4 node_3980 = tex2D(_Albedo,TRANSFORM_TEX(node_3856, _Albedo));
                float4 node_7309 = tex2D(_Albedo,TRANSFORM_TEX(node_8852, _Albedo));
                float4 node_5659 = tex2D(_Albedo,TRANSFORM_TEX(node_3376, _Albedo));
                float4 _node_1733 = tex2D(_Albedo,TRANSFORM_TEX(node_3856, _Albedo));
                float4 _node_158 = tex2D(_Albedo,TRANSFORM_TEX(node_8852, _Albedo));
                float4 _node_6794 = tex2D(_Albedo,TRANSFORM_TEX(node_3376, _Albedo));
                float3 diffuseColor = saturate((saturate(( _node_9254.rgb > 0.5 ? (1.0-(1.0-2.0*(_node_9254.rgb-0.5))*(1.0-(node_2868.r*node_3980.rgb + node_2868.g*node_7309.rgb + node_2868.b*node_5659.rgb))) : (2.0*_node_9254.rgb*(node_2868.r*node_3980.rgb + node_2868.g*node_7309.rgb + node_2868.b*node_5659.rgb)) ))*saturate(( _AOIntensity > 0.5 ? (1.0-(1.0-2.0*(_AOIntensity-0.5))*(1.0-(node_2868.r*_node_1733.a + node_2868.g*_node_158.a + node_2868.b*_node_6794.a))) : (2.0*_AOIntensity*(node_2868.r*_node_1733.a + node_2868.g*_node_158.a + node_2868.b*_node_6794.a)) )))); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
/////// Diffuse:
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
/// Final Color:
                outDiffuse = half4( diffuseColor, 1 );
                outSpecSmoothness = half4( specularColor, gloss );
                outNormal = half4( normalDirection * 0.5 + 0.5, 1 );
                outEmission = half4(0,0,0,1);
                outEmission.rgb += indirectSpecular * 1;
                outEmission.rgb += indirectDiffuse * diffuseColor;
                #ifndef UNITY_HDR_ON
                    outEmission.rgb = exp2(-outEmission.rgb);
                #endif
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Metallic;
            uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _minroughness;
            uniform float _maxroughness;
            uniform float4 _node_9254;
            uniform fixed _tiling;
            uniform fixed _offset;
            uniform float _AOIntensity;
            uniform float _fresnelintensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float3 tangentDir : TEXCOORD4;
                float3 bitangentDir : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD9;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 node_2868 = (i.normalDir*i.normalDir);
                float2 node_3856_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_3856_ty = floor(_offset * node_3856_tc_rcp.x);
                float node_3856_tx = _offset - _tiling * node_3856_ty;
                float2 node_3856 = (float2(i.posWorld.g,i.posWorld.b) + float2(node_3856_tx, node_3856_ty)) * node_3856_tc_rcp;
                float4 node_733 = tex2D(_Normal,TRANSFORM_TEX(node_3856, _Normal));
                float2 node_8852_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_8852_ty = floor(_offset * node_8852_tc_rcp.x);
                float node_8852_tx = _offset - _tiling * node_8852_ty;
                float2 node_8852 = (float2(i.posWorld.b,i.posWorld.r) + float2(node_8852_tx, node_8852_ty)) * node_8852_tc_rcp;
                float4 node_7787 = tex2D(_Normal,TRANSFORM_TEX(node_8852, _Normal));
                float2 node_3376_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_3376_ty = floor(_offset * node_3376_tc_rcp.x);
                float node_3376_tx = _offset - _tiling * node_3376_ty;
                float node_442_ang = 1.5708;
                float node_442_spd = 1.0;
                float node_442_cos = cos(node_442_spd*node_442_ang);
                float node_442_sin = sin(node_442_spd*node_442_ang);
                float2 node_442_piv = float2(0.5,0.5);
                float2 node_442 = (mul(float2(i.posWorld.r,i.posWorld.g)-node_442_piv,float2x2( node_442_cos, -node_442_sin, node_442_sin, node_442_cos))+node_442_piv);
                float2 node_3376 = (node_442 + float2(node_3376_tx, node_3376_ty)) * node_3376_tc_rcp;
                float4 node_4520 = tex2D(_Normal,TRANSFORM_TEX(node_3376, _Normal));
                float3 normalLocal = (node_2868.r*(node_733.rgb*2.0+-1.0) + node_2868.g*(node_7787.rgb*2.0+-1.0) + node_2868.b*(node_4520.rgb*2.0+-1.0));
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float node_6804 = 0.0;
                float node_9399 = saturate((_minroughness-((1.0-max(0,dot(i.normalDir, viewDirection)))*_fresnelintensity)));
                float gloss = 1.0 - (node_9399 + ( ((node_2868.r*node_733.a + node_2868.g*node_7787.a + node_2868.b*node_4520.a) - node_6804) * (_maxroughness - node_9399) ) / (1.0 - node_6804)); // Convert roughness to gloss
                float perceptualRoughness = (node_9399 + ( ((node_2868.r*node_733.a + node_2868.g*node_7787.a + node_2868.b*node_4520.a) - node_6804) * (_maxroughness - node_9399) ) / (1.0 - node_6804));
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float4 node_3980 = tex2D(_Albedo,TRANSFORM_TEX(node_3856, _Albedo));
                float4 node_7309 = tex2D(_Albedo,TRANSFORM_TEX(node_8852, _Albedo));
                float4 node_5659 = tex2D(_Albedo,TRANSFORM_TEX(node_3376, _Albedo));
                float4 _node_1733 = tex2D(_Albedo,TRANSFORM_TEX(node_3856, _Albedo));
                float4 _node_158 = tex2D(_Albedo,TRANSFORM_TEX(node_8852, _Albedo));
                float4 _node_6794 = tex2D(_Albedo,TRANSFORM_TEX(node_3376, _Albedo));
                float3 diffuseColor = saturate((saturate(( _node_9254.rgb > 0.5 ? (1.0-(1.0-2.0*(_node_9254.rgb-0.5))*(1.0-(node_2868.r*node_3980.rgb + node_2868.g*node_7309.rgb + node_2868.b*node_5659.rgb))) : (2.0*_node_9254.rgb*(node_2868.r*node_3980.rgb + node_2868.g*node_7309.rgb + node_2868.b*node_5659.rgb)) ))*saturate(( _AOIntensity > 0.5 ? (1.0-(1.0-2.0*(_AOIntensity-0.5))*(1.0-(node_2868.r*_node_1733.a + node_2868.g*_node_158.a + node_2868.b*_node_6794.a))) : (2.0*_AOIntensity*(node_2868.r*_node_1733.a + node_2868.g*_node_158.a + node_2868.b*_node_6794.a)) )))); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                half surfaceReduction;
                #ifdef UNITY_COLORSPACE_GAMMA
                    surfaceReduction = 1.0-0.28*roughness*perceptualRoughness;
                #else
                    surfaceReduction = 1.0/(roughness*roughness + 1.0);
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                indirectSpecular *= surfaceReduction;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Metallic;
            uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _minroughness;
            uniform float _maxroughness;
            uniform float4 _node_9254;
            uniform fixed _tiling;
            uniform fixed _offset;
            uniform float _AOIntensity;
            uniform float _fresnelintensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float3 tangentDir : TEXCOORD4;
                float3 bitangentDir : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 node_2868 = (i.normalDir*i.normalDir);
                float2 node_3856_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_3856_ty = floor(_offset * node_3856_tc_rcp.x);
                float node_3856_tx = _offset - _tiling * node_3856_ty;
                float2 node_3856 = (float2(i.posWorld.g,i.posWorld.b) + float2(node_3856_tx, node_3856_ty)) * node_3856_tc_rcp;
                float4 node_733 = tex2D(_Normal,TRANSFORM_TEX(node_3856, _Normal));
                float2 node_8852_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_8852_ty = floor(_offset * node_8852_tc_rcp.x);
                float node_8852_tx = _offset - _tiling * node_8852_ty;
                float2 node_8852 = (float2(i.posWorld.b,i.posWorld.r) + float2(node_8852_tx, node_8852_ty)) * node_8852_tc_rcp;
                float4 node_7787 = tex2D(_Normal,TRANSFORM_TEX(node_8852, _Normal));
                float2 node_3376_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_3376_ty = floor(_offset * node_3376_tc_rcp.x);
                float node_3376_tx = _offset - _tiling * node_3376_ty;
                float node_442_ang = 1.5708;
                float node_442_spd = 1.0;
                float node_442_cos = cos(node_442_spd*node_442_ang);
                float node_442_sin = sin(node_442_spd*node_442_ang);
                float2 node_442_piv = float2(0.5,0.5);
                float2 node_442 = (mul(float2(i.posWorld.r,i.posWorld.g)-node_442_piv,float2x2( node_442_cos, -node_442_sin, node_442_sin, node_442_cos))+node_442_piv);
                float2 node_3376 = (node_442 + float2(node_3376_tx, node_3376_ty)) * node_3376_tc_rcp;
                float4 node_4520 = tex2D(_Normal,TRANSFORM_TEX(node_3376, _Normal));
                float3 normalLocal = (node_2868.r*(node_733.rgb*2.0+-1.0) + node_2868.g*(node_7787.rgb*2.0+-1.0) + node_2868.b*(node_4520.rgb*2.0+-1.0));
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float node_6804 = 0.0;
                float node_9399 = saturate((_minroughness-((1.0-max(0,dot(i.normalDir, viewDirection)))*_fresnelintensity)));
                float gloss = 1.0 - (node_9399 + ( ((node_2868.r*node_733.a + node_2868.g*node_7787.a + node_2868.b*node_4520.a) - node_6804) * (_maxroughness - node_9399) ) / (1.0 - node_6804)); // Convert roughness to gloss
                float perceptualRoughness = (node_9399 + ( ((node_2868.r*node_733.a + node_2868.g*node_7787.a + node_2868.b*node_4520.a) - node_6804) * (_maxroughness - node_9399) ) / (1.0 - node_6804));
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float4 node_3980 = tex2D(_Albedo,TRANSFORM_TEX(node_3856, _Albedo));
                float4 node_7309 = tex2D(_Albedo,TRANSFORM_TEX(node_8852, _Albedo));
                float4 node_5659 = tex2D(_Albedo,TRANSFORM_TEX(node_3376, _Albedo));
                float4 _node_1733 = tex2D(_Albedo,TRANSFORM_TEX(node_3856, _Albedo));
                float4 _node_158 = tex2D(_Albedo,TRANSFORM_TEX(node_8852, _Albedo));
                float4 _node_6794 = tex2D(_Albedo,TRANSFORM_TEX(node_3376, _Albedo));
                float3 diffuseColor = saturate((saturate(( _node_9254.rgb > 0.5 ? (1.0-(1.0-2.0*(_node_9254.rgb-0.5))*(1.0-(node_2868.r*node_3980.rgb + node_2868.g*node_7309.rgb + node_2868.b*node_5659.rgb))) : (2.0*_node_9254.rgb*(node_2868.r*node_3980.rgb + node_2868.g*node_7309.rgb + node_2868.b*node_5659.rgb)) ))*saturate(( _AOIntensity > 0.5 ? (1.0-(1.0-2.0*(_AOIntensity-0.5))*(1.0-(node_2868.r*_node_1733.a + node_2868.g*_node_158.a + node_2868.b*_node_6794.a))) : (2.0*_AOIntensity*(node_2868.r*_node_1733.a + node_2868.g*_node_158.a + node_2868.b*_node_6794.a)) )))); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Metallic;
            uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _minroughness;
            uniform float _maxroughness;
            uniform float4 _node_9254;
            uniform fixed _tiling;
            uniform fixed _offset;
            uniform float _AOIntensity;
            uniform float _fresnelintensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float3 node_2868 = (i.normalDir*i.normalDir);
                float2 node_3856_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_3856_ty = floor(_offset * node_3856_tc_rcp.x);
                float node_3856_tx = _offset - _tiling * node_3856_ty;
                float2 node_3856 = (float2(i.posWorld.g,i.posWorld.b) + float2(node_3856_tx, node_3856_ty)) * node_3856_tc_rcp;
                float4 node_3980 = tex2D(_Albedo,TRANSFORM_TEX(node_3856, _Albedo));
                float2 node_8852_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_8852_ty = floor(_offset * node_8852_tc_rcp.x);
                float node_8852_tx = _offset - _tiling * node_8852_ty;
                float2 node_8852 = (float2(i.posWorld.b,i.posWorld.r) + float2(node_8852_tx, node_8852_ty)) * node_8852_tc_rcp;
                float4 node_7309 = tex2D(_Albedo,TRANSFORM_TEX(node_8852, _Albedo));
                float2 node_3376_tc_rcp = float2(1.0,1.0)/float2( _tiling, _tiling );
                float node_3376_ty = floor(_offset * node_3376_tc_rcp.x);
                float node_3376_tx = _offset - _tiling * node_3376_ty;
                float node_442_ang = 1.5708;
                float node_442_spd = 1.0;
                float node_442_cos = cos(node_442_spd*node_442_ang);
                float node_442_sin = sin(node_442_spd*node_442_ang);
                float2 node_442_piv = float2(0.5,0.5);
                float2 node_442 = (mul(float2(i.posWorld.r,i.posWorld.g)-node_442_piv,float2x2( node_442_cos, -node_442_sin, node_442_sin, node_442_cos))+node_442_piv);
                float2 node_3376 = (node_442 + float2(node_3376_tx, node_3376_ty)) * node_3376_tc_rcp;
                float4 node_5659 = tex2D(_Albedo,TRANSFORM_TEX(node_3376, _Albedo));
                float4 _node_1733 = tex2D(_Albedo,TRANSFORM_TEX(node_3856, _Albedo));
                float4 _node_158 = tex2D(_Albedo,TRANSFORM_TEX(node_8852, _Albedo));
                float4 _node_6794 = tex2D(_Albedo,TRANSFORM_TEX(node_3376, _Albedo));
                float3 diffColor = saturate((saturate(( _node_9254.rgb > 0.5 ? (1.0-(1.0-2.0*(_node_9254.rgb-0.5))*(1.0-(node_2868.r*node_3980.rgb + node_2868.g*node_7309.rgb + node_2868.b*node_5659.rgb))) : (2.0*_node_9254.rgb*(node_2868.r*node_3980.rgb + node_2868.g*node_7309.rgb + node_2868.b*node_5659.rgb)) ))*saturate(( _AOIntensity > 0.5 ? (1.0-(1.0-2.0*(_AOIntensity-0.5))*(1.0-(node_2868.r*_node_1733.a + node_2868.g*_node_158.a + node_2868.b*_node_6794.a))) : (2.0*_AOIntensity*(node_2868.r*_node_1733.a + node_2868.g*_node_158.a + node_2868.b*_node_6794.a)) ))));
                float specularMonochrome;
                float3 specColor;
                diffColor = DiffuseAndSpecularFromMetallic( diffColor, _Metallic, specColor, specularMonochrome );
                float4 node_733 = tex2D(_Normal,TRANSFORM_TEX(node_3856, _Normal));
                float4 node_7787 = tex2D(_Normal,TRANSFORM_TEX(node_8852, _Normal));
                float4 node_4520 = tex2D(_Normal,TRANSFORM_TEX(node_3376, _Normal));
                float node_6804 = 0.0;
                float node_9399 = saturate((_minroughness-((1.0-max(0,dot(i.normalDir, viewDirection)))*_fresnelintensity)));
                float roughness = (node_9399 + ( ((node_2868.r*node_733.a + node_2868.g*node_7787.a + node_2868.b*node_4520.a) - node_6804) * (_maxroughness - node_9399) ) / (1.0 - node_6804));
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
