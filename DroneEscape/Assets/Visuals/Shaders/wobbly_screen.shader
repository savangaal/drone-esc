// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True;n:type:ShaderForge.SFN_Final,id:4795,x:33369,y:32755,varname:node_4795,prsc:2|emission-4666-OUT,alpha-3076-OUT;n:type:ShaderForge.SFN_VertexColor,id:2053,x:32919,y:33067,varname:node_2053,prsc:2;n:type:ShaderForge.SFN_Color,id:797,x:32317,y:32634,ptovrint:True,ptlb:Color 2,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Time,id:7659,x:31919,y:32865,varname:node_7659,prsc:2;n:type:ShaderForge.SFN_Vector1,id:4925,x:31982,y:32782,varname:node_4925,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:9695,x:32145,y:32782,varname:node_9695,prsc:2|A-4925-OUT,B-7659-TTR;n:type:ShaderForge.SFN_Sin,id:7968,x:32317,y:32782,varname:node_7968,prsc:2|IN-9695-OUT;n:type:ShaderForge.SFN_RemapRange,id:7477,x:32474,y:32782,varname:node_7477,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-7968-OUT;n:type:ShaderForge.SFN_Lerp,id:7120,x:32752,y:32756,varname:node_7120,prsc:2|A-48-RGB,B-797-RGB,T-7477-OUT;n:type:ShaderForge.SFN_Color,id:48,x:32317,y:32461,ptovrint:False,ptlb:Color 1,ptin:_Color1,varname:node_48,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:9177,x:32563,y:32996,ptovrint:False,ptlb:Scanline Texture,ptin:_ScanlineTexture,varname:node_9177,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-4277-UVOUT;n:type:ShaderForge.SFN_Add,id:4666,x:33024,y:32854,varname:node_4666,prsc:2|A-7120-OUT,B-9177-RGB,C-9645-RGB;n:type:ShaderForge.SFN_Panner,id:4277,x:32363,y:33037,varname:node_4277,prsc:2,spu:1,spv:0.3|UVIN-6249-UVOUT,DIST-7930-OUT;n:type:ShaderForge.SFN_TexCoord,id:6249,x:32155,y:33122,varname:node_6249,prsc:2,uv:0;n:type:ShaderForge.SFN_Divide,id:7930,x:32155,y:32994,varname:node_7930,prsc:2|A-7477-OUT,B-8908-OUT;n:type:ShaderForge.SFN_Vector1,id:8908,x:31982,y:33013,varname:node_8908,prsc:2,v1:1;n:type:ShaderForge.SFN_Color,id:9645,x:32719,y:33067,ptovrint:False,ptlb:Scanline Color,ptin:_ScanlineColor,varname:node_9645,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:1091,x:32762,y:33243,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_1091,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:3;n:type:ShaderForge.SFN_Multiply,id:3076,x:33131,y:33017,varname:node_3076,prsc:2|A-9177-R,B-2053-R,C-1091-OUT;proporder:48-797-1091-9645-9177;pass:END;sub:END;*/

Shader "Craft/wobbly_screen" {
    Properties {
        _Color1 ("Color 1", Color) = (0.5,0.5,0.5,1)
        _TintColor ("Color 2", Color) = (0.5,0.5,0.5,1)
        _Opacity ("Opacity", Range(0, 3)) = 0
        _ScanlineColor ("Scanline Color", Color) = (0.5,0.5,0.5,1)
        _ScanlineTexture ("Scanline Texture", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _TintColor;
            uniform float4 _Color1;
            uniform sampler2D _ScanlineTexture; uniform float4 _ScanlineTexture_ST;
            uniform float4 _ScanlineColor;
            uniform float _Opacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 node_7659 = _Time + _TimeEditor;
                float node_7477 = (sin((2.0*node_7659.a))*0.5+0.5);
                float3 node_7120 = lerp(_Color1.rgb,_TintColor.rgb,node_7477);
                float2 node_4277 = (i.uv0+(node_7477/1.0)*float2(1,0.3));
                float4 _ScanlineTexture_var = tex2D(_ScanlineTexture,TRANSFORM_TEX(node_4277, _ScanlineTexture));
                float3 emissive = (node_7120+_ScanlineTexture_var.rgb+_ScanlineColor.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,(_ScanlineTexture_var.r*i.vertexColor.r*_Opacity));
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
