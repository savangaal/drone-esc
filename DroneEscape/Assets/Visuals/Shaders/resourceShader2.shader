// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:33073,y:32663,varname:node_2865,prsc:2|emission-3768-OUT,alpha-990-OUT;n:type:ShaderForge.SFN_Color,id:6665,x:32471,y:32520,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5019608,c2:0.5019608,c3:0.5019608,c4:1;n:type:ShaderForge.SFN_Fresnel,id:7472,x:32374,y:33000,varname:node_7472,prsc:2|EXP-9674-OUT;n:type:ShaderForge.SFN_Slider,id:818,x:31498,y:32809,ptovrint:False,ptlb:Dissolve amount,ptin:_Dissolveamount,varname:node_818,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_RemapRange,id:8758,x:31850,y:32810,varname:node_8758,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-818-OUT;n:type:ShaderForge.SFN_Tex2d,id:7487,x:31807,y:32587,varname:node_7487,prsc:2,ntxv:0,isnm:False|UVIN-6854-OUT,TEX-8661-TEX;n:type:ShaderForge.SFN_Add,id:6235,x:32117,y:32724,varname:node_6235,prsc:2|A-7487-R,B-8758-OUT;n:type:ShaderForge.SFN_Clamp01,id:5702,x:32287,y:32724,varname:node_5702,prsc:2|IN-6235-OUT;n:type:ShaderForge.SFN_OneMinus,id:4588,x:32471,y:32737,varname:node_4588,prsc:2|IN-5702-OUT;n:type:ShaderForge.SFN_Multiply,id:9452,x:32765,y:32637,varname:node_9452,prsc:2|A-6665-RGB,B-2041-OUT,C-4588-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2041,x:32471,y:32683,ptovrint:False,ptlb:Bloom intensity,ptin:_Bloomintensity,varname:node_2041,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Slider,id:9674,x:32001,y:33019,ptovrint:False,ptlb:Rim power,ptin:_Rimpower,varname:node_9674,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:4;n:type:ShaderForge.SFN_Panner,id:4199,x:31344,y:32514,varname:node_4199,prsc:2,spu:1,spv:1|UVIN-1690-OUT,DIST-9769-OUT;n:type:ShaderForge.SFN_TexCoord,id:2693,x:31987,y:32508,varname:node_2693,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Tex2d,id:309,x:31601,y:32368,varname:node_309,prsc:2,ntxv:0,isnm:False|UVIN-4199-UVOUT,TEX-8661-TEX;n:type:ShaderForge.SFN_Add,id:6854,x:32245,y:32455,varname:node_6854,prsc:2|A-8940-OUT,B-2693-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:8940,x:31987,y:32346,varname:node_8940,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-309-RGB;n:type:ShaderForge.SFN_Tex2dAsset,id:8661,x:31300,y:32288,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_8661,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Power,id:3039,x:32552,y:32953,varname:node_3039,prsc:2|VAL-8076-OUT,EXP-7472-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8076,x:32374,y:32941,ptovrint:False,ptlb:Rim opacity,ptin:_Rimopacity,varname:node_8076,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Slider,id:7123,x:30822,y:32846,ptovrint:False,ptlb:Pan speed,ptin:_Panspeed,varname:node_7123,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Time,id:1545,x:30822,y:32675,varname:node_1545,prsc:2;n:type:ShaderForge.SFN_RemapRange,id:3871,x:31142,y:32801,varname:node_3871,prsc:2,frmn:0,frmx:1,tomn:10,tomx:0|IN-7123-OUT;n:type:ShaderForge.SFN_Divide,id:9769,x:31344,y:32672,varname:node_9769,prsc:2|A-1545-T,B-3871-OUT;n:type:ShaderForge.SFN_Multiply,id:990,x:32757,y:32813,varname:node_990,prsc:2|A-4588-OUT,B-3039-OUT;n:type:ShaderForge.SFN_Multiply,id:3768,x:32906,y:32715,varname:node_3768,prsc:2|A-9452-OUT,B-3039-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:4550,x:30492,y:32322,varname:node_4550,prsc:2;n:type:ShaderForge.SFN_ToggleProperty,id:1374,x:30689,y:32494,ptovrint:False,ptlb:WorldSpace,ptin:_WorldSpace,varname:node_1374,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_Append,id:3719,x:30689,y:32322,cmnt:Stairs,varname:node_3719,prsc:2|A-4550-X,B-4550-Y;n:type:ShaderForge.SFN_TexCoord,id:4161,x:30689,y:32123,cmnt:Resource,varname:node_4161,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Lerp,id:1690,x:30889,y:32302,varname:node_1690,prsc:2|A-4161-UVOUT,B-3719-OUT,T-1374-OUT;proporder:6665-2041-818-9674-8661-8076-7123-1374;pass:END;sub:END;*/

Shader "DroneEscape/resourceShader2" {
    Properties {
        _Color ("Color", Color) = (0.5019608,0.5019608,0.5019608,1)
        _Bloomintensity ("Bloom intensity", Float ) = 0
        _Dissolveamount ("Dissolve amount", Range(0, 1)) = 0
        _Rimpower ("Rim power", Range(0, 4)) = 0
        _Texture ("Texture", 2D) = "white" {}
        _Rimopacity ("Rim opacity", Float ) = 0
        _Panspeed ("Pan speed", Range(0, 1)) = 0
        [MaterialToggle] _WorldSpace ("WorldSpace", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _Dissolveamount;
            uniform float _Bloomintensity;
            uniform float _Rimpower;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _Rimopacity;
            uniform float _Panspeed;
            uniform fixed _WorldSpace;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_1545 = _Time;
                float2 node_4199 = (lerp(i.uv0,float2(i.posWorld.r,i.posWorld.g),_WorldSpace)+(node_1545.g/(_Panspeed*-10.0+10.0))*float2(1,1));
                float4 node_309 = tex2D(_Texture,TRANSFORM_TEX(node_4199, _Texture));
                float2 node_6854 = (node_309.rgb.r+i.uv0);
                float4 node_7487 = tex2D(_Texture,TRANSFORM_TEX(node_6854, _Texture));
                float node_4588 = (1.0 - saturate((node_7487.r+(_Dissolveamount*2.0+-1.0))));
                float node_3039 = pow(_Rimopacity,pow(1.0-max(0,dot(normalDirection, viewDirection)),_Rimpower));
                float3 emissive = ((_Color.rgb*_Bloomintensity*node_4588)*node_3039);
                float3 finalColor = emissive;
                return fixed4(finalColor,(node_4588*node_3039));
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _Dissolveamount;
            uniform float _Bloomintensity;
            uniform float _Rimpower;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _Rimopacity;
            uniform float _Panspeed;
            uniform fixed _WorldSpace;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : SV_Target {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 node_1545 = _Time;
                float2 node_4199 = (lerp(i.uv0,float2(i.posWorld.r,i.posWorld.g),_WorldSpace)+(node_1545.g/(_Panspeed*-10.0+10.0))*float2(1,1));
                float4 node_309 = tex2D(_Texture,TRANSFORM_TEX(node_4199, _Texture));
                float2 node_6854 = (node_309.rgb.r+i.uv0);
                float4 node_7487 = tex2D(_Texture,TRANSFORM_TEX(node_6854, _Texture));
                float node_4588 = (1.0 - saturate((node_7487.r+(_Dissolveamount*2.0+-1.0))));
                float node_3039 = pow(_Rimopacity,pow(1.0-max(0,dot(normalDirection, viewDirection)),_Rimpower));
                o.Emission = ((_Color.rgb*_Bloomintensity*node_4588)*node_3039);
                
                float3 diffColor = float3(0,0,0);
                o.Albedo = diffColor;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
